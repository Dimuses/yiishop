<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'dmstr/yii2-db' => 
  array (
    'name' => 'dmstr/yii2-db',
    'version' => '0.3.0.0',
    'alias' => 
    array (
      '@dmstr/db' => $vendorDir . '/dmstr/yii2-db/db',
      '@dmstr/console' => $vendorDir . '/dmstr/yii2-db/console',
    ),
  ),
  'dmstr/yii2-helpers' => 
  array (
    'name' => 'dmstr/yii2-helpers',
    'version' => '0.3.0.0',
    'alias' => 
    array (
      '@dmstr/helpers' => $vendorDir . '/dmstr/yii2-helpers/src',
    ),
  ),
  'dmstr/yii2-bootstrap' => 
  array (
    'name' => 'dmstr/yii2-bootstrap',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@dmstr/bootstrap' => $vendorDir . '/dmstr/yii2-bootstrap',
    ),
  ),
  'schmunk42/yii2-giiant' => 
  array (
    'name' => 'schmunk42/yii2-giiant',
    'version' => '0.7.1.0',
    'alias' => 
    array (
      '@schmunk42/giiant' => $vendorDir . '/schmunk42/yii2-giiant/src',
    ),
    'bootstrap' => 'schmunk42\\giiant\\Bootstrap',
  ),
  'rsr/yii2-button-dropdown-sorter' => 
  array (
    'name' => 'rsr/yii2-button-dropdown-sorter',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@rsr/yii2' => $vendorDir . '/rsr/yii2-button-dropdown-sorter',
    ),
  ),
  'yii2mod/yii2-enum' => 
  array (
    'name' => 'yii2mod/yii2-enum',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@yii2mod/enum' => $vendorDir . '/yii2mod/yii2-enum',
    ),
  ),
  'yii2mod/yii2-editable' => 
  array (
    'name' => 'yii2mod/yii2-editable',
    'version' => '1.1.3.0',
    'alias' => 
    array (
      '@yii2mod/editable' => $vendorDir . '/yii2mod/yii2-editable',
    ),
  ),
  'yii2mod/yii2-behaviors' => 
  array (
    'name' => 'yii2mod/yii2-behaviors',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@yii2mod/behaviors' => $vendorDir . '/yii2mod/yii2-behaviors',
    ),
  ),
  'yii2mod/yii2-comments' => 
  array (
    'name' => 'yii2mod/yii2-comments',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@yii2mod/comments' => $vendorDir . '/yii2mod/yii2-comments',
    ),
  ),
);
