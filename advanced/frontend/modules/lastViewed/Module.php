<?php

namespace app\modules\LastViewed;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\LastViewed\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
