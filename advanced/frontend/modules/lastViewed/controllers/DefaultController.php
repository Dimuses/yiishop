<?php

namespace app\modules\LastViewed\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use frontend\models\ActiveRecord\Product;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex($settings)
    {
        if(!$settings['status'])
            return false;

        $provider = new ActiveDataProvider([
            'query' => Product::find()
                ->with('productAttributes.attributeInfo.attributeDescription')
                ->joinwith('productDescription')
                ->with('productStickers.stickerDescription')
                ->with('productComments')
                ->where(['product.product_id' => json_decode(Yii::$app->session->get('lastViewed'))]),
            'pagination' => [
                'pageSize' => $settings['perPage'],
            ],
        ]);
        return $this->render('index',['provider' => $provider]);
    }
}
