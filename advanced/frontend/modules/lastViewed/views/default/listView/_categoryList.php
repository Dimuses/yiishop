<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="product-item-list ">
  <div class="product-img">
    <img width="150" src="<?=Yii::getAlias('@web').'/img/' . $model->image?>" alt="">
  </div>

  <div class="desc_block">
    <div class="product-title"><?= Html::encode($model->productName) ?></div>

    <p class="product_desc">
      <?= $model->productName ?>
    </p>
  </div>
  <div class="product-btn">
    <button type="button" class="btn btn-info">Купить</button>
  </div>
  <div class="clear"></div>
</div>