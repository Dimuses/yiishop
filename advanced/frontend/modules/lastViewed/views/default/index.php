<?
use yii\widgets\ListView;
use yii\helpers\VarDumper;
?>
<div class="lastViewed">
<? if ($provider->getTotalCount()): ?>
    <h2 class="title">Последние просмотренные:</h2>

    <?= ListView::widget([
      'dataProvider' => $provider,
      'itemView' => 'ListView/_categoryGrid',
      'layout' => '{items}',
      'pager' => [
        'options' => ['class' => 'bootstrap-styles pagination center-pag']
      ],
    ]); ?>
    <div class="clear"></div>
<? endif ?>
</div>