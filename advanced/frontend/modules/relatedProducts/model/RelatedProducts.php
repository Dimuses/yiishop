<?php

namespace app\modules\relatedProducts\model;

use frontend\models\ActiveRecord\Product;
use Yii;

/**
 * This is the model class for table "related_products".
 *
 * @property integer $product_id
 * @property integer $related_product_id
 * @property integer $relation_id
 */
class RelatedProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'related_products';
    }

    public function fields()
    {
        return [
            'related_product_id'
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['related_product_id', 'relation_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'related_product_id' => 'Related Product ID',
            'relation_id' => 'Relation ID',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(),['product_id' => 'product_id']);
    }
}
