<?php

namespace app\modules\relatedProducts;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\module\relatedProducts\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
