<?php

namespace app\modules\relatedProducts\controllers;

use app\modules\relatedProducts\model\RelatedProducts;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;
use frontend\models\ActiveRecord\Product;


class DefaultController extends Controller
{
  public function actionIndex($settings)
  {
    if(!$settings['status'])
      return false;

    $product_id = Yii::$app->request->getQueryParam('id');


    $provider = new ActiveDataProvider([
      'query' => Product::find()
        ->Joinwith('productAttributes.attributeInfo.attributeDescription')
        ->Joinwith('productDescription')
        ->Joinwith('productStickers.stickerDescription')
        ->with('productComments')
        ->where(['product.product_id' => RelatedProducts::find()->select('related_product_id')->where(['product_id' => $product_id])->asArray()->column()]),

    ]);

    //VarDumper::dump($relatedProducts,10,1);
    return $this->render('index',['provider' => $provider]);
  }
}
