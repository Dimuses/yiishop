<?php

namespace app\modules\Top\controllers;

use frontend\models\ActiveRecord\Module;
use frontend\models\ActiveRecord\Product;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex($settings)
    {
        if(!$settings['status'])
            return false;

        $top = (new Product())->getTopOrdered(Yii::$app->request->getQueryParam('id'), $settings['limit']);
        return $this->render('index',['top' => $top, 'limit' => $settings['limit']]);
    }
}
