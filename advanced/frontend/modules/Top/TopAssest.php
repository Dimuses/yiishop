<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 23.05.2016
 * Time: 10:35
 */

namespace app\modules\Top;

use yii\web\AssetBundle;

class TopAssest extends  AssetBundle
{
  public $sourcePath = '@app/modules/Top/assets';
  public $js = [

  ];
  public $css =[
    'css/top_module.css'
  ];
  public $publishOptions = [
    'forceCopy'=>true,
  ];

  public $depends = [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
  ];
}