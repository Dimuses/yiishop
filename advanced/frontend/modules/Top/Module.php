<?php

namespace app\modules\Top;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Top\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
