<?
use yii\widgets\ListView;
use yii\helpers\VarDumper;
use yii\helpers\Html;
use app\modules\Top\TopAssest;

$count = count($top);
TopAssest::register($this);
?>
<div class="topOrdered">
<? if ($count > 0): ?>
  <h2 class="title">Топ-<?=$limit?>:</h2>
  <? //VarDumper::dump($top, 10, 1)?>

  <?for($i = 0; $i < $count; $i++):?>
    <div class="top-ordered-<?=$i + 1?>">
    <div class="circle"><?=$i + 1?></div>
    <?= Html::a(
      Html::img(
        Yii::getAlias('@web') .'/img/' . $top[$i]['image'],
        ['width' => '28%',]
      ),
      ['category/product','id' => $top[$i]['product_id']],[])?>
      <div class="right-part">
        <?= Html::a($top[$i]['name'],['category/product','id' => $top[$i]['product_id']])?>
        <div class="price">
          <?= Yii::$app->formatter->asDecimal($top[$i]['price'])?><span class="curr"> грн</span>
          <div class="rating">
            <img src="<?= Yii::getAlias('@web') . '/img/5stars.jpg' ?>" alt="">
          </div>
        </div>
      </div>
    </div>
  <?endfor?>
<? endif ?>
</div>