<?php

namespace app\modules\Category\controllers;

use yii\web\Controller;
use frontend\models\ActiveRecord\Category;

class DefaultController extends Controller
{
    public function actionIndex($settings)
    {
        $items = (new Category())->prepareMenuItems();
        return $this->render('index', ['items' => $items]);
    }
}
