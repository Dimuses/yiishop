<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 11.05.2016
 * Time: 13:07
 */

namespace app\modules\Category;
use yii\web\AssetBundle;

class CategoryAsset extends AssetBundle
{
  public $sourcePath = '@app/modules/Category/assets';
  public $css = [
    'css/category_module.css',
  ];


}