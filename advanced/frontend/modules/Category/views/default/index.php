<?php
use yii\widgets\Menu;
use app\modules\Category\CategoryAsset;

CategoryAsset::register($this);
?>
<div class="category_menu">
<?= Menu::widget([
  'items' => $items,
  'activeCssClass'=>'active',
  'linkTemplate' => '<div class="head"><a href="{url}">{label}</a></div>',
  'submenuTemplate' => '<ul>{items}</ul>',
  'options' => [
    'data'=>'menu',
  ],
]);
?>
</div>
