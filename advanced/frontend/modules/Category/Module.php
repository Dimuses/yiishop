<?php

namespace app\modules\Category;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Category\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
