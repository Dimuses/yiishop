<?php

namespace app\modules\Filter;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Filter\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
