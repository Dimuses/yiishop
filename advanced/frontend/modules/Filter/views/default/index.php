<?
use app\modules\Filter\FilterAsset;
use yii\web\View;
use yii\helpers\VarDumper;
use yii\helpers\BaseHtml;

FilterAsset::register($this);
?>
<div class="product_filter">
	<h1>Фильтр</h1>

	<?=BaseHtml::beginForm(['filter/default/submit'], 'post')?>
	<p class="filter-title">Цена:</p>
	<div class="slider-wrapper" style="text-align: center;">
		<div id="inputs-wrapper">
			<label for="minPrice"></label>
			<input type="text" id="minPrice" name="Filter[minP]" value="<?=$filter['minPrice']?>"/>
			<label for="maxPrice">-</label>
			<input type="text" id="maxPrice"  name="Filter[maxP]" value="<?=$filter['maxPrice'] ?>"/> <span class="curr f12">грн</span>
			<button type="submit" class="btn btn-default" >Ок</button>
		</div>
		<div id="slider"></div>
	</div>
	<hr>
	<? if (count($manufacturers) > 0): ?>
	<div><p class="filter-title">Производитель:</p><!--<i class="fa fa-chevron-up" aria-hidden="true"></i>--></div>
	<? foreach ($manufacturers as $item): ?>
		<label>
			<input type="checkbox" name="Filter[man][]" <?= in_array($item['manufacturer_id'], $filter['man']) ? 'checked' : '' ?> value="<?= $item['manufacturer_id'] ?>"><?= $item['name'] ?>
		</label>
	<?endforeach?>
	<? endif ?>
	<? foreach ($attributes as $k => $attribute):?>
		<hr>
		<div><p class="filter-title"><?=$attribute['name']?>:</p><!--<i class="fa fa-chevron-up" aria-hidden="true"></i>--></div>
		<?foreach($attribute['values'] as $id => $subAttribute):?>
			<label>
				<input type="checkbox" name="Filter[<?=$k?>][]" <?=in_array($id, $filter['attributes']) ? 'checked' : ''?> value="<?=$id?>"><?=$subAttribute?>
			</label>
		<? endforeach?>
	<? endforeach?>
	<div id="reset-wrapper"><button type="submit" name="Filter[reset]" class="btn btn-default" >Сбросить</button></div>

	<hr>
	<?=BaseHtml::endForm()?>
</div>


<?
$sliderJS = <<<JS
jQuery("#slider").slider({
	min: 0,
	max: 1000,
	values: [{$filter['minPrice']}, {$filter['maxPrice']}],
	range: true,
	stop: function(event, ui) {
		jQuery("input#minPrice").val(jQuery("#slider").slider("values",0));
		jQuery("input#maxPrice").val(jQuery("#slider").slider("values",1));
    },
    slide: function(event, ui){
		jQuery("input#minPrice").val(jQuery("#slider").slider("values",0));
		jQuery("input#maxPrice").val(jQuery("#slider").slider("values",1));
    }
});
jQuery("input#minPrice").change(function(){
	var value1=jQuery("input#minPrice").val();
	var value2=jQuery("input#maxPrice").val();

    if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		jQuery("input#minPrice").val(value1);
	}
	jQuery("#slider").slider("values",0,value1);
});

jQuery("input#maxPrice").change(function(){
	var value1=jQuery("input#minPrice").val();
	var value2=jQuery("input#maxPrice").val();

	if (value2 > 1000) { value2 = 1000; jQuery("input#maxPrice").val(1000)}

	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery("input#maxPrice").val(value2);
	}
	jQuery("#slider").slider("values",1,value2);
});

JS;

$formStone = <<<JS
$("input[type=checkbox], input[type=radio]").checkbox({customClass: "cust-check"});
$("input[type=number]").number({theme: "fs-light"});
JS;

$this->registerJs($sliderJS, View::POS_END);
$this->registerJs($formStone, View::POS_END)
?>


