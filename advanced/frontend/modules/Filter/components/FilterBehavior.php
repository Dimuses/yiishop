<?php
namespace app\modules\Filter\components;

use app\modules\Filter\controllers\DefaultController;
use frontend\controllers\MyController;
use yii\base\Controller;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\View;
use Yii;


class FilterBehavior extends \yii\base\Behavior{

  public function events()
  {
    return [
      MyController::EVENT_BEFORE_RENDER => 'filter',
    ];
  }

  public function filter($event)
  {
    $filterStr = Yii::$app->request->getQueryParam('f');
    if(!empty($filterStr)){
      $filterArr = explode(';', $filterStr);
    }
    $preparedFilterArr = [];
    if(!empty($filterArr))
      foreach ($filterArr as  $i){
        $posOne = strpos($i, '=');
        $k = substr($i, 0, $posOne);
        $v = substr($i, $posOne + 1);

        if(strpos($v , ','))
          $preparedFilterArr[$k] = explode(',', $v);
        else{
          $preparedFilterArr[$k] = ($k == 'man') ? [$v] : $v;
        }
      }
    array_pop($preparedFilterArr);


    $minPrice = !empty($preparedFilterArr['minP']) ? (int)$preparedFilterArr['minP'] : 0;
    unset($preparedFilterArr['minP']);

    $maxPrice = !empty($preparedFilterArr['maxP']) ? (int)$preparedFilterArr['maxP'] : 1000;
    unset($preparedFilterArr['maxP']);

    $manufacturer = !empty($preparedFilterArr['man']) ? $preparedFilterArr['man'] : [];
    unset($preparedFilterArr['man']);

    $attributes = [];

    foreach ($preparedFilterArr as $k => $v ) {
      if(is_array($v))
        foreach($v as $subV){
          $attributes[] = $subV;
        }
      else
        $attributes[] = $v;
    }

    if(get_class($event->sender) == DefaultController::className()){
      $event->params['filter']['minPrice'] = $minPrice;
      $event->params['filter']['maxPrice'] = $maxPrice;
      $event->params['filter']['man'] = $manufacturer;
      $event->params['filter']['attributes'] = $attributes;
    }elseif(Yii::$app->controller->action->id == 'category'){
      $event->params['provider']->query->andFilterWhere(['between', 'price', $minPrice, $maxPrice]);
      $event->params['provider']->query->andFilterWhere(['manufacturer_id' => $manufacturer]);
      $event->params['provider']->query->andFilterWhere(['product_attribute_id' => $attributes]);
    }
  }
}