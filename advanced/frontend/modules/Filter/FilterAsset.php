<?php

namespace app\modules\Filter;
use yii\web\AssetBundle;

class FilterAsset extends AssetBundle
{
  public $sourcePath = '@app/modules/Filter/assets';
  public $js = [
    '//code.jquery.com/ui/1.11.4/jquery-ui.js',
  ];
  public $css =[
    'css/filter_module.css'
  ];
  public $publishOptions = [
    'forceCopy'=>true,
  ];

  public $depends = [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset',
    'frontend\assets\FormstoneAsset'
  ];

}