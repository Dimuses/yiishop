<?php

namespace app\modules\Filter\controllers;

use app\modules\Filter\components\FilterBehavior;
use app\modules\Filter\models\FilterModel;
use frontend\controllers\CategoryController;
use frontend\models\ActiveRecord\Manufacturer;
use yii\helpers\VarDumper;
use frontend\controllers\MyController;
use Yii;
use yii\web\Request;

class DefaultController extends MyController
{
  public function behaviors()
  {
    return [
      FilterBehavior::className()
    ];
  }

  public function actionIndex()
  {
    //TODO:: Сделать атач поведения по условию if(Yii::$app->controller->action->id), in_array($settings)
    Yii::$app->controller->attachBehavior('filterBehavior', FilterBehavior::className());

    $filterModel = new FilterModel();
    $attributes = $filterModel->prepareAttributes(Yii::$app->request->getQueryParam('id'));
    $manufacturers = $filterModel->prepareManufacturers(Yii::$app->request->getQueryParam('id'));

    return $this->render('index', [
      'manufacturers' => $manufacturers,
      'attributes' => $attributes
    ]);
  }

  public function actionSubmit()
  {
    $post = Yii::$app->request->post();
    $url = Yii::$app->request->referrer;

    $filterModel = new FilterModel();
    $preparedUrl = $filterModel->prepareUrl($url, $post['Filter']);

    $this->redirect($preparedUrl ? $preparedUrl : $url);
  }
}
