<?php
namespace app\modules\Filter\models;

use frontend\models\ActiveRecord\Product;
use frontend\models\ActiveRecord\ProductAttribute;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\VarDumper;

class FilterModel extends Model
{
  public $minP;
  public $maxP;
  public $man;

  public function prepareUrl($url, $post)
  {
    $preparedUrl = '';
    $pos = (strpos($url, '?f=')) ? strpos($url, '?f=') : strpos($url, '&f=') ? strpos($url, '&f=') : false;

    if ($pos) {

      $preparedUrl .= substr($url, 0, $pos);
      $posSec = (strpos($url, '&', $pos + 1)) ? strpos($url, '&', $pos + 1) : false;
      if ($posSec) {
        $preparedUrl .= substr($url, $posSec);
      }
    }else{
      $preparedUrl .= $url;
    }

    if(!isset($post['reset'])){
      $preparedUrl .= strpos($preparedUrl, '?') ? '&f=' : '?f=';

      if ($preparedUrl) {
        if (!empty($post)) {
          foreach ($post as $k => $v) {
            if (is_array($v)) {
              $preparedUrl .= "$k=";
              foreach ($v as $item) {
                if (next($v))
                  $preparedUrl .= $item . ',';
                else
                  $preparedUrl .= $item . ';';
              }
            } else {
              $preparedUrl .= "$k=$v;";
            }
          }
        }
      }
    }
    return $preparedUrl;
  }

  public function prepareAttributes($category_id)
  {
    $attributes = (new Query())
      ->select(['pa.attribute_id','name','text', 'product_attribute_id'])

      ->from(['product_to_category ptc'])
      ->leftJoin('product_attribute pa', 'pa.product_id = ptc.product_id')
      ->leftJoin('attribute_description ad', 'ad.attribute_id = pa.attribute_id' )
      ->where(['pa.language_id' => 1, 'category_id' => $category_id])
      ->all();

    $prepareAttributes = [];

    foreach($attributes as $attribute){
      $prepareAttributes[$attribute['attribute_id']]['name'] = $attribute['name'];
      $prepareAttributes[$attribute['attribute_id']]['values'][$attribute['product_attribute_id']] = $attribute['text'];
    }
    return $prepareAttributes;
  }

  public function prepareManufacturers($category_id)
  {
    return (new Query())
      ->select(['m.manufacturer_id', 'name'])
      ->distinct()
      ->from(['product p'])
      ->leftJoin('manufacturer m', 'p.manufacturer_id = m.manufacturer_id')
      ->leftJoin('product_to_category ptc', 'ptc.product_id = p.product_id')
      ->where(['ptc.category_id' => $category_id])
      ->andwhere('m.manufacturer_id != 0')
      ->all();
  }

}

