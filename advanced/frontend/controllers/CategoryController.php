<?php

namespace frontend\controllers;

use frontend\models\ActiveRecord\CategoryDescription;
use frontend\models\ActiveRecord\Module;
use frontend\models\ActiveRecord\SeoTemplates;
use frontend\models\ActiveRecord\Category;
use frontend\models\ActiveRecord\Product;

use frontend\models\CategoryForm;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use frontend\components\MySort;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use yii2mod\comments\models\CommentModel;
use frontend\models\ProductForm;
use app\modules\Filter\components\FilterBehavior;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use Yii;

class CategoryController extends MyController
{
  public function __construct($id, $module, $config = [])
  {
    if(!Yii::$app->session->isActive)
      Yii::$app->session->open();
    parent::__construct($id, $module, $config);
  }

  public function behaviors()
  {
    return [];
  }

  public function actionCategory($id)
  {
    $model = new CategoryForm();
    $request = Yii::$app->request;
    $params = $request->getQueryParams();

    if ($request->isPost && $model->load($request->post()))
    {
      Yii::$app->session->set('perPage', $model->perPage);
    }
    elseif($request->isGet)
    {
      $model->perPage = $params['per-page'] ? $params['per-page'] : $model->perPage;
      Yii::$app->session->set('perPage', $model->perPage);
    }
    else{
      $model->perPage = Yii::$app->session->get('perPage');
    }

    $sort = new MySort([
      'attributes' => [
        'productName'=>[
          'asc' => ['product_description.Name' => SORT_ASC],
          'desc' => ['product_description.Name' => SORT_DESC],
          'default' => SORT_DESC,
          'label' => 'Имя',
        ],
        'price'=>[
          'asc' => ['price' => SORT_ASC],
          'desc' => ['price' => SORT_DESC],
          'default' => SORT_DESC,
          'label' => 'Цена',
        ],
      ],
      'enableMultiSort' => true
    ]);

    $provider = new ActiveDataProvider([
      'query' => Product::find()
        ->joinwith('productAttributes.attributeInfo.attributeDescription')
        ->joinwith('productDescription')
        ->with('productStickers.stickerDescription')
        ->with('productComments')
        ->joinWith('productCategories')
        ->where(['category_id' => $id])
        ->orderBy($sort->orders),
      'pagination' => [
        'pageSize' => $model->perPage ? $model->perPage : 20 ,
      ],
    ]);

    $moduleModel = new Module();
    $modules = $moduleModel->prepareModules('category/category');

    $categoryModel = new Category();

    return $this->render('category', [
      'provider' => $provider,
      'model' => $model,
      'sort' => $sort,
      'modules' => $modules,
      'breadcrumbs' => $categoryModel->prepareBreadcrumbs($id),
      'h1' => CategoryDescription::findOne(['category_id' => $id])->name
    ]);
  }

  public function actionProduct($id)
  {
    $attributesModel = new Product();
    $product = Product::findOne($id);

    $commentModel = new CommentModel(['id' => $id]);
    $attributes = $attributesModel->prepareAttributes($id);
    $options = $attributesModel->prepareOptions($id);

    $productForm = new ProductForm();
    $productForm->attachOptionsRules($options);
    /*
     * Last Viewed
     * */
    $lastViewed = Yii::$app->session->get('lastViewed');
    $lastViewed = (array)json_decode($lastViewed);
    $lastViewed[] = $id;
    Yii::$app->session->set('lastViewed', json_encode(array_values(array_unique((array)$lastViewed))));
    $seoModel = new SeoTemplates();

    //TODO:: Организовать в  behavior метатеги

    $metaTags = $seoModel->getReadyTemplates('product', $id);
    $moduleModel = new Module();
    $modules = $moduleModel->prepareModules('category/product');

    return $this->render('product', [
      'model' => $product,
      'attributes' => $attributes,
      'comment' => $commentModel,
      'options' => $options,
      'productForm' => $productForm,
      'modules' => $modules,
      'metaTags' => $metaTags

    ]);
  }
  public function actionSearch($q = '')
  {
    $model = new CategoryForm();
    $request = Yii::$app->request;
    $params = $request->getQueryParams();

    if ($request->isPost && $model->load($request->post()))
    {
      Yii::$app->session->set('perPage', $model->perPage);
    }
    elseif($request->isGet)
    {
      $model->perPage = $params['per-page'] ? $params['per-page'] : $model->perPage;
      Yii::$app->session->set('perPage', $model->perPage);
    }
    else{
      $model->perPage = Yii::$app->session->get('perPage');
    }

    /** @var \himiklab\yii2\search\Search $search */
    $search = Yii::$app->search;
    $searchData = $search->find($q); // Search by full index.
    //$searchData = $search->find($q, ['model' => 'ProductDescription']); // Search by index provided only by model `page`.

    $dataProvider = new ArrayDataProvider([
      'allModels' => $searchData['results'],
      'pagination' => ['pageSize' => 10],
    ]);

    $productIds = (new Category())->getSearchedIds($searchData['results']);

    $sort = new MySort([
      'attributes' => [
        'productName'=>[
          'asc' => ['product_description.Name' => SORT_ASC],
          'desc' => ['product_description.Name' => SORT_DESC],
          'default' => SORT_DESC,
          'label' => 'Имя',
        ],
        'price'=>[
          'asc' => ['price' => SORT_ASC],
          'desc' => ['price' => SORT_DESC],
          'default' => SORT_DESC,
          'label' => 'Цена',
        ],
      ],
      'enableMultiSort' => true
    ]);

    $provider = new ActiveDataProvider([
      'query' => Product::find()
        ->joinwith('productAttributes.attributeInfo.attributeDescription')
        ->joinwith('productDescription')
        ->with('productStickers.stickerDescription')
        ->with('productComments')
        ->joinWith('productCategories')
        ->where(['product.product_id' => $productIds])
        ->orderBy($sort->orders),
      'pagination' => [
        'pageSize' => 20 ,
      ],
    ]);

    $moduleModel = new Module();
    $modules = $moduleModel->prepareModules('category/search');

    return $this->render('found', [
      'provider' => $provider,
      'model' => $model,
      'sort' => $sort,
      'modules' => $modules,
      'query' => $searchData['query']
    ]);


  }

}
