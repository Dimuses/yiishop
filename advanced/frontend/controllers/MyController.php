<?php

namespace frontend\controllers;

use app\components\events\BeforeRenderEvent;
use yii\web\Controller;

class MyController extends Controller
{
  const EVENT_BEFORE_RENDER = 'beforeRender';

  public function render($view, $params = [])
  {
    $event = new BeforeRenderEvent();
    $event->params = $params;

    self::trigger(self::EVENT_BEFORE_RENDER, $event);
    return parent::render($view, $event->params);
  }

}