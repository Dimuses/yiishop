<?php
namespace frontend\components;
use \yii\base\Component;
use frontend\models\ActiveRecord\DiscountCode;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Cart extends \yii\base\Object{


  public $model;

  public $qty;

  public $total;

  public $cartParam = 'cart';

  public $cart;

  public $items;

  protected $_session;

  protected $_coupon;

  public function init()
  {
    parent::init();

    $this->_session = \Yii::$app->session->open();
    $cartModelObj = new $this->model;

    /* @var $cartModelObj \frontend\models\ActiveRecord\Cart*/
    $this->cart = $cartModelObj->getCart(session_id());
    $this->items =  ArrayHelper::map($this->cart, 'cart_id', 'quantity');
    $this->qty = $this->totalQty();

    $promoCode = \Yii::$app->session->get('promocode');
    $code = DiscountCode::findOne(['str_code' => $promoCode]);
    /* @var $code \frontend\models\ActiveRecord\DiscountCode*/
    if($code){
      if($code->isActive())
        $this->_coupon = $code;
    }
    $this->total = $this->totalPrice();
  }

  public function totalQty(){
    $total = 0;
    //print_r($this->items); die;
    if(is_array($this->cart)){
      foreach($this->cart as $k => $v ){
        $total += $v['quantity'];
      }
    }
    return $total;
  }
  public function totalPrice(){
    $totalPrice = 0;

    if(!empty($this->items)){
      /* @var $cartModelObj \frontend\models\ActiveRecord\Cart*/
      $cartModelObj = new $this->model;
      $all = $cartModelObj->prepareItems($this->items, $this->_coupon);

      if($this->_coupon){
        foreach ($all as $item){
          $totalPrice += isset($item['discount']) ? $item['price_with_discount'] * $item['quantity'] : $item['price'] * $item['quantity'];
        }
      }else{
        foreach ($all as $item){
          $totalPrice += $item['price'] * $item['quantity'];
        }
      }
    }

    return $totalPrice;
  }
}