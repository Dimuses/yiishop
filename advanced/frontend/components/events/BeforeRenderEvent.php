<?php

namespace app\components\events;

use yii\base\Event;

class BeforeRenderEvent extends Event
{
  public $params;
}