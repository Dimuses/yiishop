<?php
namespace frontend\components;

use \yii\base\Component;
class Menu extends \yii\base\Object{

  public $model;

  public function init()
  {
    parent::init();
  }

  public function top(){
    $class = $this->model;
    $model = new $class();
    return $model->top();
  }


}