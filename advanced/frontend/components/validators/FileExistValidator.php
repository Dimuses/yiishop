<?php

namespace app\components\validators;

use yii\helpers\VarDumper;
use yii\validators\Validator;

class FileExistValidator extends Validator
{
  public function validateAttribute($model, $attribute)
  {

    foreach ($model as $item) {
      VarDumper::dump($item,20,1);
      if(file_exists($item->$attribute)){
        $this->addError($item, $attribute, 'Файл не сушествует');;
      }
    }
  }
}