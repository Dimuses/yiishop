<?php

namespace frontend\components;

use yii\widgets\LinkSorter;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class DropSort extends LinkSorter
{
  public $labelAsc = 'ASC';
  public $labelDesc = 'DESC';
  public $DDL = [
    'labelSelect' =>'SORT by',
    'id' => 'DDLSort'
  ];

  protected function renderSortLinks()
  {
    $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
    $links = [];

    foreach ($attributes as $key => $name) {
      //echo $key;
      $links[] = [
        'value' => $this->sort->link($key, true),
        'label' =>$name['label'] ? ($name['label'] . ' ' . $this->labelAsc) : ($key . ' ' . $this->labelAsc),
      ];
      $links[] = [
        'value' => $this->sort->link($key, false),
        'label' =>$name['label'] ? ($name['label'] . ' ' . $this->labelDesc) : ($key . ' ' . $this->labelDesc),
      ];
    }
    //print_r($links);
    $label = Html::Label($this->DDL['labelSelect'], $this->DDL['id'], ['class' => 'control-label']);
    return  Html::tag('div', $label .Html::dropDownList('s_id', \Yii::$app->request->url, ArrayHelper::map($links, 'value', 'label'), $this->options));
  }
}