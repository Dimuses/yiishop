<?php

namespace frontend\components\helpers;


use yii\base\Component;
use Yii;

class ShopHelper extends Component
{
  public static function discountOfSum($sum, $percent)
  {
    return Yii::$app->formatter->asDecimal($sum / 100 * $percent);
  }

  public static function sumWithDiscount($sum, $percent)
  {
    return Yii::$app->formatter->asDecimal($sum - ($sum / 100 * $percent));
  }
}