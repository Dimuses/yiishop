<?php
namespace frontend\components;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

class ListViewShop extends ListView{


  public $pageSize = ['25', '50', '75', '100'];
  public $filterLabel;

  public function renderSection($name)
  {
    switch ($name) {
      case '{summary}':
        return $this->renderSummary();
      case '{items}':
        return $this->renderItems();
      case '{pager}':
        return $this->renderPager();
      case '{sorter}':
        return $this->renderSorter();
      case '{filter}':
        return $this->renderFilter();
      default:
        return false;
    }
  }

  public function renderFilter(){

    $droplist = Html::tag('label', $this->filterLabel,['for' => 'f-per-page']) .
    Html::dropDownList('count-per-page', null, $this->pageSize, ['id' => 'f-per-page']);


    return $block = Html::tag('div', $droplist);
  }

  public function renderSorter(){
    $sort = Html::dropDownList('sort','', [
      'popularity DESC'=>"По популярности",
      'name ASC'=>'Имя(А->Я)',
      'name DESC'=>'Имя(Я->А)',
      'price ASC'=>'Цена(по наростанию)',
      'price DESC'=>'Цена(по убыванию)'

    ],['id'=>'sortDrop']);

    return $sort;
  }
}