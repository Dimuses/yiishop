<?php
return
  [
    'yii\web\JqueryAsset' => [
      'sourcePath' => null,   // не опубликовывать комплект
      'js' => [
        (YII_ENV_DEV) ? '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js' : '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
      ]
    ],
  ];