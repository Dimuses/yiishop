<?php
Yii::setAlias('@webroot', '/advanced/frontend/web');
Yii::setAlias('@web', '/');


$params = array_merge(
  require(__DIR__ . '/../../common/config/params.php'),
  require(__DIR__ . '/../../common/config/params-local.php'),
  require(__DIR__ . '/params.php'),
  require(__DIR__ . '/params-local.php')
);

return [
  'id' => 'app-frontend',
  'basePath' => dirname(__DIR__),
  'aliases' =>[
    '@menu' => '@app/views/menu',
    '@js' => '/js',
    '@css' => '/css',

  ],
  'bootstrap' => ['log', 'session'],
  'controllerNamespace' => 'frontend\controllers',
  'language' => 'ru-RU',
  'modules' => [
    'comment' => [
      'class' => 'yii2mod\comments\Module'
    ],
    'last-viewed' => [
        'class' => 'app\modules\lastViewed\Module',
    ],
    'filter' => [
      'class' => 'app\modules\Filter\Module',
    ],
  ],
  'components' => [
    'user' => [
      'identityClass' => 'common\models\User',
      'enableAutoLogin' => true,
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],

    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,

    ],
    'assetManager' => [
      'bundles' => require(__DIR__ . '/' . (YII_ENV_PROD ? 'prod-assets.php' : 'dev-assets.php')),
      'appendTimestamp' => true,
      'linkAssets' => true,
    ],
    'db' => [
      'class' => '\yii\db\Connection',
      'dsn' => 'mysql:host=127.0.0.1;dbname=yii2advanced',
      'username' => 'root',
      'password' => '',
      'charset' => 'utf8',
    ],
    'menu' =>[
      'class' => 'frontend\components\Menu',
      'model' => 'frontend\models\Menu'
    ],
    'cart' =>[
      'class' => 'frontend\components\Cart',
      'model' => 'frontend\models\ActiveRecord\Cart'
    ],

    'session' =>[
      'class' => 'yii\web\Session',
    ],
    'i18n' => [
      'translations' => [
        'yii2mod.comments' => [
          'class' => 'yii\i18n\PhpMessageSource',
          'basePath' => '@app/messages',
          //'sourceLanguage' => 'en-US',
          'fileMap' => [
            'yii2mod.comments' => 'comments.php',
          ],
        ],
      ],
    ],
    'search' => [
      'class' => 'himiklab\yii2\search\Search',
      'models' => [
        'frontend\models\ActiveRecord\ProductDescription',
      ],
    ],


  ],
  'params' => $params,
  'layout' => 'main-left-sidebar',
  'controllerMap' => [
    'comments' => 'yii2mod\comments\controllers\ManageController'
  ]

];
