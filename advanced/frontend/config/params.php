<?php
return [
    'adminEmail' => 'admin@example.com',
    'picSize' => [
        'category'=>[
          'grid' => [
            'w' => 130,
            'h' => 130
          ],
          'list' =>[
            'w' => 145,
            'h' => 145
          ]
        ],
        'cart'=>[
          'w' => 70,
          'h' => 70
        ],
        'confirm'=>[
          'w' => 50,
          'h' => 50
        ],
    ],
];
