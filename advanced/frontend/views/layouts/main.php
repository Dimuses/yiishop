<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\FontAwesomeAsset;
use common\widgets\Alert;


AppAsset::register($this);
FontAwesomeAsset::register($this);

?>
<?php $this->beginPage();?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrapper">
  <div id="header">
    <div id="logo">
      <a href="">
        <img src="<?=Yii::getAlias('@web')?>/img/logo.png" alt=""></a>
    </div>
    <p class="slogan">Мы продаём то, <span class="red">что купили бы сами</span></p>
    <?= $this->render('_search_form', ['text' => '']) ?>

    <div class="clear"></div>
    <p class="working_time first">
      Время работы Пн - ПТ<br>
      с 8-00 до 20-00
    </p>
    <p class="working_time second">
      В выходные дни<br>
      с  9-00 до 17-00
    </p>
    <p class="phones">
      8 (495) 510 27 27<br>
      8 (800) 100 05 56
    </p>
    <div id="cart">
      <span id="head"><?=Html::a('Корзина',['cart/index'])?></span><br>
      <span id="cnt-total">
        товары: <span class="qty"><?= Yii::$app->cart->qty ?></span><br>
        сумма: <span class="total"><?=Yii::$app->formatter->asDecimal(Yii::$app->cart->total) ?></span> грн.
      </span><br>
    </div>

    <?php
    echo Menu::widget([
      'items' =>Yii::$app->menu->top(),
      'activeCssClass'=>'active',
      'options' => [
        'id'=>'header_menu',
        'class' => 'hor_menu',
        'data'=>'menu',
      ],
    ]);
    ?>
  </div>
  <div id="main">
    <div id="under_menu">
      <?= Breadcrumbs::widget([
        'homeLink' => [
          'label' => Yii::t('yii', 'Главная'),
          'url' => Yii::$app->homeUrl,
        ],
        'options' => ['class' => 'breadcrumbs'],
        'itemTemplate' => "{link}<i class='fa fa-long-arrow-right'></i>\n",
        'activeItemTemplate' => "{link}\n",
        'tag' => 'div',
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ])?>


      <div id="cat_title">О компании</div>

    </div>
    <div id="all">
      <div id="out">
        <?if (isset($this->blocks['top']))
          echo $this->blocks['top'];
        ?>
        <?=$content?>
      </div>
      <div id="bottom">
        <?if (isset($this->blocks['bottom']))
          echo $this->blocks['bottom'];
        ?>
      </div>

      <div id="akcii">
        <div id="arrow_l"><img src="<?=Yii::getAlias('@web')?>/img/arrow-l.png" alt=""></div>
        <div id="arrow_r"><img src="<?=Yii::getAlias('@web')?>/img/arrow-r.png" alt=""></div>
        <h2 class="title">Акции</h2>
        <div class="inner">
          <div class="product">
            <img src="<?=Yii::getAlias('@web')?>/img/product.png" alt="">
            <h3>MP46FDA</h3>
            <p class="desc">Краткоре описание техники...
              Краткоре описание техники...
              Краткоре описание техники...
            </p>
          </div>
          <div class="product">
            <img src="<?=Yii::getAlias('@web')?>/img/product.png" alt="">
            <h3>MP46FDA</h3>
            <p class="desc">Краткоре описание техники...
              Краткоре описание техники...
              Краткоре описание техники...
            </p>
          </div>
          <div class="product">
            <img src="<?=Yii::getAlias('@web')?>/img/product.png" alt="">
            <h3>MP46FDA</h3>
            <p class="desc">Краткоре описание техники...
              Краткоре описание техники...
              Краткоре описание техники...
            </p>
          </div>
          <div class="product">
            <img src="<?=Yii::getAlias('@web')?>/img/product.png" alt="">
            <h3>MP46FDA</h3>
            <p class="desc">Краткоре описание техники...
              Краткоре описание техники...
              Краткоре описание техники...
            </p>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div id="advantages">
        <h2 class="title">Преимущества</h2>
        <div class="col-1">
          <table>
            <tr>
              <td>
                <img src="<?=Yii::getAlias('@web')?>/img/garanty.png" alt="">
              </td>
              <td>
                <h3>Гарантия качества</h3>
                <p>
                  Новый ручной блендер для хозяек, знающих цену каждой минуте.
                  Его не только приятно держать в руках, несложно мыть и жалко убирать на место.
                </p>
              </td>
            </tr>
            <tr>
              <td>
                <img src="<?=Yii::getAlias('@web')?>/img/del.png" alt="">
              </td>
              <td>
                <h3>Доставка по всей Беларусии</h3>
                <p>
                  Новый ручной блендер для хозяек, знающих цену каждой минуте.
                  Его не только приятно держать в руках, несложно мыть и жалко убирать на место.
                </p>
              </td>
            </tr>
            <tr>
              <td>
                <img src="<?=Yii::getAlias('@web')?>/img/service.png" alt="">
              </td>
              <td><h3>Надёжный сервис</h3>
                <p>
                  Новый ручной блендер для хозяек, знающих цену каждой минуте.
                  Его не только приятно держать в руках, несложно мыть и жалко убирать на место.
                </p></td>
            </tr>
          </table>
        </div>
        <div class="col-2">
          <table>
            <tr>
              <td>
                <img src="<?=Yii::getAlias('@web')?>/img/price.png" alt="">
              </td>
              <td>
                <h3>Честные цены</h3>
                <p>
                  Новый ручной блендер для хозяек, знающих цену каждой минуте.
                  Его не только приятно держать в руках, несложно мыть и жалко убирать на место.
                </p>
              </td>
            </tr>
            <tr>
              <td>
                <img src="<?=Yii::getAlias('@web')?>/img/kredit.png" alt="">
              </td>
              <td>
                <h3>Кредит «не выходя из дома»</h3>
                <p>
                  Новый ручной блендер для хозяек, знающих цену каждой минуте.
                  Его не только приятно держать в руках, несложно мыть и жалко убирать на место.
                </p>
              </td>
            </tr>
            <tr>
              <td>
                <img src="<?=Yii::getAlias('@web')?>/img/sale.png" alt="">
              </td>
              <td>
                <h3>Выгодные покупки</h3>
                <p>
                  Новый ручной блендер для хозяек, знающих цену каждой минуте.
                  Его не только приятно держать в руках, несложно мыть и жалко убирать на место.
                </p>
              </td>
            </tr>
          </table>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>

  <div id="footer">
    <ul id="footer_menu" class="hor_menu">
      <li class="active">О магазине</li>
      <li><a href="">Оплата</a></li>
      <li><a href="">Доставка</a></li>
      <li><a href="">Подержка</a></li>
      <li><a href="">Новости</a></li>
      <li><a href="">Контакты</a></li>
    </ul>
    <div class="footer_bottom">
      <div id="counters">
        <img src="<?=Yii::getAlias('@web')?>/img/cnt.png" alt="">
        <img src="<?=Yii::getAlias('@web')?>/img/ga.png" alt="">
      </div>
      <p class="working_time" style="margin-left:50px">
        Время работы Пн - ПТ<br>
        с 8-00 до 20-00
      </p>
      <p class="phones">
        8 (495) 510 27 27<br>
        8 (800) 100 05 56
      </p>
      <div id="studio">
        <img src="<?=Yii::getAlias('@web')?>/img/studio_logo.png" alt="">
        <p class="f14">Разработка сайта</p>
      </div>
    </div>
    <div class="clear"></div>

  </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
