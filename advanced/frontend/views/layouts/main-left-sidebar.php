<?php $this->beginContent('@app/views/layouts/main.php') ?>

  <div id="left">
    <?php if (isset($this->blocks['left']))
      echo $this->blocks['left']
    ?>
    <div id="requisites">
      <div class="title">Реквизиты</div>
      <p>
        г Минск ул. Энтузиастов 84<br>
        ИНН 3316007270, кор. счёт<br>
        30101810200000000716,<br>
        Р/с 40703810612260000004
      </p>
    </div>
  </div>
<?if (isset($this->blocks['right'])) { ?>
  <!--Вывод правого блока-->
  <div id="right">
    <?= $this->blocks['right'] ?>
  </div>
<?}?>
  <!--Конец правого блока-->
  <div id="center">
    <?= $content ?>
  </div>

  <div class="clear"></div>

<?$this->endContent(); ?>