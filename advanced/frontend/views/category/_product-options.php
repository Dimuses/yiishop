<?
/* @var yii\base\Model $optionsForm Модель валидации формы опций */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


  foreach ($options as  $option) {
    if($option['type'] == 'select'){
      echo $form
        ->field($optionsForm, 'option_' . $option['product_option_id'])
        ->dropDownList(ArrayHelper::map($option['product_option_value'], 'product_option_value_id', 'name'), ['prompt' => '--Выберите--'])
        ->label($option['name']);
    }elseif($option['type'] == 'radio'){
      echo $form
        ->field($optionsForm, 'option_' . $option['product_option_id'])
        ->radioList(ArrayHelper::map($option['product_option_value'], 'product_option_value_id', 'name'))
        ->label($option['name']);
    }elseif($option['type'] == 'checkbox'){
      echo $form
        ->field($optionsForm, 'option_' . $option['product_option_id'])
        ->checkboxList(ArrayHelper::map($option['product_option_value'], 'product_option_value_id', 'name'))
        ->label($option['name']);
    }elseif($option['type'] == 'datetime'){
      echo $form->field($optionsForm, 'option_' . $option['product_option_id'])->widget(DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
      ])->label($option['name']);
    }
  }
?>
