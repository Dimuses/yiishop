<?
/* @var array $attributes Атрибуты продукта  */
/* @var array $options Опции продукта  */
/* @var object $comment Модель комментариев  */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\assets\FormstoneAsset;
use yii\web\View;
use common\widgets\Alert;
$this->title = $metaTags['title'];
//\yii\helpers\VarDumper::dump($metaTags, 10, 1 ); die;

foreach ($metaTags['meta'] as $name => $content) {
  $this->registerMetaTag([
    'name' => $name,
    'content' => $content
  ]);
}

FormstoneAsset::register($this);
$this->registerCssFile('@css/product.css');
$this->registerJsFile('//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js');

echo Alert::widget();
?>

<div id="prd-wrapper">
  <div class="product-left">
    <div class="prd-image">
      <div class="new-label">Новинка</div>
      <div class="main-img"><img src="../img/<?= $model->image ?>" alt="" width="370"></div>
    </div>
    <div id="color-but-wrapper">
      <div id="color-select">
        <span>Выберите цвет:</span>
        <ul>
          <li class="active"><img src="../img/colors/red.png" alt=""></li>
          <li><img src="../img/colors/green.png" alt=""></li>
          <li><img src="../img/colors/blue.png" alt=""></li>
          <li><img src="../img/colors/yellow.png" alt=""></li>
        </ul>
      </div>
      <div class="zoom"><img src="../img/zoom.png" alt=""></div>
      <div class="clear"></div>
      <div id="img-slider">
        <div class="slider-arrow"><img src="../img/slider-arrow-l.png" alt=""></div>
        <div><img src="../img/slider-item.jpg" alt=""></div>
        <div><img src="../img/slider-item.jpg" alt=""></div>
        <div><img src="../img/slider-item.jpg" alt=""></div>
        <div class="slider-arrow"><img src="../img/slider-arrow-r.png" alt=""></div>
      </div>
    </div>
  </div>
  <div class="product-right">
    <div id="prd-wrapper-right">
      <div class="prd-title"><?= $model->productName?></div>
      <div class="prd-sku">Артикул: <?= $model->model?></div>
      <div class="prd-color">Цвет: Слоновая кость</div>
      <div class="prd-short-txt">
        <?=$model->productDescription->short_description?>
      </div>
      <div id="attributes_short">
        <?
        foreach($attributes as $group => $attrs ){?>
          <div class="new-label grey"><?=$group?></div>
          <div class="prd-char-tbl">
            <?
            foreach ($attrs as  $attribute => $value ) {?>
              <div class="row">
                <div class="td"><?=$attribute?></div>
                <div class="td"><?=$value?></div>
              </div>
            <?}?>
          </div>
        <?}?>
      </div>
      <!--Опции-->
      <?$form = ActiveForm::begin([
        'id' => 'option-form',
        'action' => ['cart/add']
      ]);
      if(!empty($options)){?>
      <!--Рендерим доступные опции для товара-->
      <?=$this->render('_product-options', ['options' => $options, 'optionsForm' => $productForm, 'form' => $form]) ?>
      <?}?>

      <p id="tehnology">В товаре используется технология: <span class="important">SMART</span>
        Этот товар на <span class="important">Яндекс-маркете</span></p>

      <div class="share-btn">
        <img src="../img/social/share.png" alt="">
        <span>Поделиться</span>
      </div>
      <ul id="share-social">
        <li><img src="../img/social/fb.png" alt=""></li>
        <li><img src="../img/social/twitter.png" alt=""></li>
        <li><img src="../img/social/vk.png" alt=""></li>
        <li><img src="../img/social/rss.png" alt=""></li>
        <li><img src="../img/social/g+.png" alt=""></li>
      </ul>
      <div id="price-wrapper">
        <div class="prd-price">
          <?=number_format($model->price, 2, ',', ' ');?> грн
        </div>
        <?=Html::hiddenInput('product_id', $model->product_id);?>
        <?=Html::submitButton('Купить', ['class' => 'buy-btn'])?>
        <?$form->end()?>
      </div>
    </div>
  </div>
  <div class="clear"></div>

  <div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab1" data-toggle="tab">Описание</a></li>
      <li><a href="#tab2" data-toggle="tab">Все характеристики</a></li>
      <li><a href="#comments" data-toggle="tab">Отзывы</a></li>
      <li><a href="#tab4" data-toggle="tab">Файлы/Документы</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade active" id="tab1">
        <p class="desc">
          <?=$model->productDescription->description?>
        </p>
      </div>
      <div class="tab-pane fade" id="tab2">
        <div class="prd-char-tbl">
          <div class="row">
            <div class="td">Обьём плоскости для приготовления, п</div>
            <div class="td">5</div>
          </div>
          <div class="row">
            <div class="td">Приготовление под давлением</div>
            <div class="td">Да</div>
          </div>
          <div class="row">
            <div class="td">Приготовление на пару</div>
            <div class="td">Да</div>
          </div>
          <div class="row">
            <div class="td">Управление</div>
            <div class="td">Электронное</div>
          </div>
          <div class="row">
            <div class="td">Количество автоматических программ</div>
            <div class="td">11</div>
          </div>
          <div class="row">
            <div class="td">Количество уровней защиты</div>
            <div class="td">11</div>
          </div>
          <div class="row">
            <div class="td">Книга рецептов</div>
            <div class="td">Да</div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="comments">
        <?= \yii2mod\comments\widgets\Comment::widget([
          'model' => $comment,
        ]);
        ?>
      </div>
      <div class="tab-pane fade" id="tab4">
        <div class="attach-file">
          <img src="../img/file.png" alt="" align="left"> <span>Инструкция (16 mb)</span>
        </div>
        <br>
        <div class="attach-file">
          <img src="../img/file.png" alt="" align="left"> <span>Книга рецептов (4 mb)</span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clear"></div>

<!--Left block-->
<?php
$this->beginBlock('left');

$this->endBlock();
?>
<!--Right block-->
<?php $this->beginBlock('right'); ?>
<? $this->endBlock(); ?>

<!--Top block-->
<?php $this->beginBlock('top'); ?>
<? $this->endBlock(); ?>

<!--Bottom block-->
<? $this->beginBlock('bottom'); ?>
<?
if(isset($modules['content_bottom']))
  foreach($modules['content_bottom'] as $module){
    echo $module;
  }
?>
<? $this->endBlock();?>


<?= \ibrarturi\scrollup\ScrollUp::widget([
  'theme' => 'image',
]);
$formStone = <<<JS
$("input[type=checkbox], input[type=radio]").checkbox({customClass: "cust-check"});
$("input[type=number]").number({theme: "fs-light"});
JS;

$tabs = <<<JS
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').click();
}

/*
// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
})*/

JS;


$this->registerJs($formStone, View::POS_END);
$this->registerJs($tabs, View::POS_END)?>


