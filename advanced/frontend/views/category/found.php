<?
/* @var $sort  */
/* @var $provider  */
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\ListView;
use yii\widgets\LinkSorter;
use frontend\components\DropSort;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;

$query = yii\helpers\Html::encode($query);

$this->title = "Результаты поиска по запросу \"$query\"";
$this->params['breadcrumbs'][] = $this->title;

app\modules\search\SearchAssets::register($this);
$this->registerJs("jQuery('.search').highlight('{$query}');");

\yii\widgets\Pjax::begin();

echo \common\widgets\Alert::widget();

$form = ActiveForm::begin([
  'id'=> 'sorter-form',
  'options'=>[
    'data-pjax' => '1'
  ]
]);
?>

  <div class="display" data-toggle="buttons-radio">
    <span>Вид</span>
    <?=Html::Submitbutton('<i class="fa fa-th-large"></i>',
      [
        'name' => 'ProductForm[viewPage]',
        'value' => '_categoryGrid',
        'class' => $model->viewPage == '_categoryGrid' ? 'active' : ''
      ])
    ?>
    <?=Html::Submitbutton('<i class="fa fa-th-list"></i>',
      [
        'name' => 'CategoryForm[viewPage]',
        'value' => '_categoryList',
        'class' => $model->viewPage == '_categoryList' ? 'active' : ''
      ])
    ?>
  </div>

<?
echo DropSort::widget([
  'sort' => $sort,
  'attributes' => [
    'productName'=>[
      'label'=>'Имя'
    ],
    'price'=>[
      'label'=>'Цена'
    ],
  ],
  'options' =>[
    'class' => 'form-control',
    'id' => 'DDL',
    'onchange' => 'window.location = $("#DDL").val();',
  ],
  'DDL' =>[
    'labelSelect' => 'Сортировать по'
  ]
]);

echo $form->field($model, 'perPage')->dropDownList([
  '20'=>"20",
  '40'=>"40",
  '60'=>'60',
  '80'=>'80',
  '100'=>'100',
],
  [
    'id'=>'filterDrop',
    'onchange' => '$("#sorter-form").submit()'
  ]);
ActiveForm::end();
?>
<div class="search">
<?
echo ListView::widget([
  'dataProvider' => $provider,
  'itemView' => 'ListView/'. $model->viewPage,
  'layout' => '{items}{summary}{pager}',
  'pager' => [
    'options' => ['class' => 'bootstrap-styles pagination center-pag']
  ],
]);?>
</div>

<?\yii\widgets\Pjax::end();?>

  <!--Top block-->
<? $this->beginBlock('top'); ?>
<?
if(isset($modules['content_top']))
  foreach($modules['content_top'] as $module){
    echo $module;
  }
?>
<? $this->endBlock();?>

  <!--Left block-->
<? $this->beginBlock('left'); ?>
<?
if(isset($modules['column_left']))
  foreach($modules['column_left'] as $module){
    echo $module;
  }
?>
<? $this->endBlock();?>

  <!--Right block-->
<? $this->beginBlock('right'); ?>
<?
if(isset($modules['column_right']))
  foreach($modules['column_right'] as $module){
    echo $module;
  }
?>
<? $this->endBlock();?>

  <!--Bottom block-->
<? $this->beginBlock('bottom'); ?>
<?
if(isset($modules['content_bottom']))
  foreach($modules['content_bottom'] as $module){
    echo $module;
  }
?>
<? $this->endBlock();?>


<?= \ibrarturi\scrollup\ScrollUp::widget([
  'theme' => 'image',
]); ?>

<? $js ="
function addToCart(id){
    $.post('". Url::toRoute(['cart/add']) ."', {'product_id' : id}, function(data){
      console.log(data);
    });
  }
";
$this->registerJs($js, View::POS_END)?>