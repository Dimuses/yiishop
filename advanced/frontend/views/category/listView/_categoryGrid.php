<?use yii\helpers\Html;
use yii\helpers\HtmlPurifier;?>
<div class="product-item-grid" id="<?=$model->product_id?>">
  <div class="product-inner">
    <span class="promo-labels" style="background:url(http://img.allo.ua/media/credit_icons/green_15.png) no-repeat"></span>
    <? foreach ($model->productStickers as  $sticker):?>
      <span class="text-stickers" style="background-color:<?=$sticker->stickerDescription[0]->color?>"> <?=$sticker->stickerDescription[0]->name?></span>
    <?endforeach?>
    <div class="stickers-wrapper">
      <!--<img width="55" src="<?/*=Yii::getAlias('@web').'/img/stickers/discount.png'*/?>" title="Скидка" class="sticker">
    <img width="55" src="<?/*=Yii::getAlias('@web').'/img/stickers/new.png'*/?>" title="Скидка" class="sticker">-->
    </div>
    <div class="clear"></div>
    <div class="product-img">
      <?=Html::a('<img width="'.Yii::$app->params['picSize']['category']['grid']['w'] .'" height="'.Yii::$app->params['picSize']['category']['grid']['h'] .'"  src="'. Yii::getAlias('@web').'/img/' . $model->image. '" alt="">',['category/product', 'id' => $model->product_id])?>
    </div>
    <div class="prd-info-box">
      <div
        class="product-title"><?= Html::a(Html::encode($model->productName), ['category/product', 'id' => $model->product_id]) ?></div>
      <div class="sku">
        код товара: <span><?=$model->model?></span>
      </div>
      <div class="delivery-icon">
        <img src="<?= Yii::getAlias('@web') . '/img/delivery_icons.png' ?>" title="Курьерская доставка">
      </div>
      <div class="ratings">
        <img src="<?= Yii::getAlias('@web') . '/img/5stars.jpg' ?>" alt="">
        <div class="comments-count">
          <?if(count($model->productComments) > 0):?>
          <?= Html::a(count($model->productComments) . ' отзывов', ['category/product', 'id' => $model->product_id, '#' => 'comments', ]) ?>
          <?else:?>
          <span class="icon"></span>
          <?= Html::a('<span class="add"> оставить отзыв</span>', ['category/product', 'id' => $model->product_id, '#' => 'comments', ],['class' => 'discussion']) ?>
          <?endif?>
        </div>
      </div>
      <div class="price-box">
        <?if(!empty($model->old_price)):?>
        <div class="old-price">
          <?=Yii::$app->formatter->asDecimal($model->old_price)?> <span class="curr">грн</span>
        </div>
        <div class="new-price">
          <?=Yii::$app->formatter->asDecimal($model->price)?> <span class="curr">грн</span>
        </div>
        <?else:?>
        <div class="price"><?= Yii::$app->formatter->asDecimal($model->price) ?> <span class="curr">грн</span></div>
        <?endif?>
      </div>

      <div class="product-btn">
        <a href="" class="buy-btn" onclick="addToCart(<?= $model->product_id ?>); return false">Купить</a>
      </div>
    </div>
    <div class="attributes por">
      <div class="info-popup">
        <div class="content">
          <div class="attr-container">
            <p class="link-compare-wrap" onclick="return {product_id: 78985}">
              <span id="compare-span-1373529497" title="Добавить к сравнению" class="check-box check-off"
                onclick="_gaq.push(['_trackEvent', 'Catalog', _pageAnalytics.category + ' | ' + _pageAnalytics.parent_category, 'add-to-comparison-catalog']); sendCompare('http://allo.ua/ajax_compare/catalog/add/product/78985/', this, {get: 'remove', updateMessage: 'Обновление списка...'})">
	            </span>
              <a id="compare-1373529497" name="compare-78985" href="javascript:" class="link-compare"
                 onclick="_gaq.push(['_trackEvent', 'Catalog', _pageAnalytics.category + ' | ' + _pageAnalytics.parent_category, 'add-to-comparison-catalog']); sendCompare('http://allo.ua/ajax_compare/catalog/add/product/78985/', this, {get: 'remove', updateMessage: 'Обновление списка...'})">Добавить
                к сравнению</a>
            </p>

            <div class="compare-button-block">
              <div id="compare-button-78985"></div>
            </div>
            <div class="attr-content">
              <? foreach ($model->productAttributes as  $item):?>
                <p><span class="span1"><?=$item->attributeInfo[0]->attributeDescription[0]->name?>:</span> <span class="span2"> <?=$item->text?>;</span></p>
              <?endforeach?>
              <?if(empty($model->productAttributes)):?>
              <p><span class="span1">3G:</span> <span class="span2"> Есть;</span></p>

              <p><span class="span1">Диагональ дисплея:</span> <span class="span2"> 9,7";</span></p>

              <p><span class="span1">Разрешение дисплея:</span> <span class="span2"> 2048x1536;</span></p>

              <p><span class="span1">Тип матрицы:</span> <span class="span2"> IPS;</span></p>

              <p><span class="span1">Тип дисплея:</span> <span class="span2"> Емкостный;</span></p>
              <?endif?>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
