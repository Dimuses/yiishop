<?php

/* @var $this yii\web\View */
use yii\helpers\VarDumper;
use yii\helpers\Html;

$this->title = 'О нас';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This is the About page. You may modify the following file to customize its content:</p>
    <?php VarDumper::dump($_SERVER, 10 , true);?>
    <code><?= __FILE__ ?></code>
</div>
