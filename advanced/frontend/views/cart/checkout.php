<?
use yii\helpers\Html;

$this->registerCssFile('@css/cart.css');
//\yii\helpers\VarDumper::dump($orderFields,100,1);
?>
<div id="confirm-wrapper">
  <h4 id="thk-title"><i class="fa fa-check custom-check" aria-hidden="true"></i>Спасибо, за ваш заказ!</h4>
  <p>Номер вашего заказа: <span id="order-id"><?=$order_id?></span></p>
  <p>
    Спасибо что выбрали "shop name". Наши менеджеры свяжутся с вами в ближайшее время. Если у вас остались какие либо вопросы,
    мы на них с радость ответим по тел.&nbsp;&nbsp;&nbsp;<i class="fa fa-mobile fa-custom" aria-hidden="true"></i>8(091)374-76-34
  </p>
  <div class="row check-head">
    <div class="border col-xs-4">
     <!-- <h4 class="check-title">Вы выбрали:</h4>-->
      <div id="check-r" class="bg-dashed">
        <div class="checkout-field"><i class="fa fa fa-user fa-custom" aria-hidden="true"></i><span class="b f13">Имя:</span><span><?=$orderFields['name']?></span></div>
        <div class="checkout-field"><i class="fa fa-truck fa-custom" aria-hidden="true"></i><span class="b f13">Доставка:</span><span><?=$orderFields['delivery']['name']?></span></div>
        <div class="checkout-field"><i class="fa fa-credit-card fa-custom" aria-hidden="true"></i><span class="b f13">Оплата:</span><span><?=$orderFields['payment']['name']?></span></div>
        <div class="checkout-field"><i class="fa fa-map-marker fa-custom" aria-hidden="true"></i><span class="b f13">Адресс:</span><span><?=$orderFields['city'] .', ' . $orderFields['street'] . ', ' . $orderFields['house']. ', ' . $orderFields['apartment']?></span></div>
        <div class="checkout-field"><i class="fa fa-phone fa-custom" aria-hidden="true"></i><span class="b f13">Тел:</span><span><?=$orderFields['phone']?></span></div>
        <div class="checkout-field"><i class="fa fa-envelope fa-custom" aria-hidden="true"></i><span class="b f13">E-mail:</span><span><?=$orderFields['email']?></span></div>
      </div>
    </div>
    <div class="border col-xs-8">
      <!--<h4 class="check-title">Вы купили:</h4>-->

      <table class="cart" cellpadding="0" cellspacing="0">
        <thead>
        <tr>
          <td></td>
          <td></td>
          <td>Цена</td>
          <? if($promocode):?><td class="discount">Скидка(<?=$promocode->code_percent?>%)</td><?endif?>
          <td>Кол-во</td>
          <td>Итого</td>
          <td></td>
        </tr>
        </thead>
        <tbody>
        <?foreach ($items as $item):?>
        <tr id="<?=$item['cart_id']?>">
          <td><img width="<?=Yii::$app->params['picSize']['confirm']['w']?>" src="<?= Yii::getAlias('@web') . '/img/' . $item['image']?>" alt=""></td>
          <td class="main-info">
            <span class="prd-title f12"><?=Html::a(Html::encode($item['name']),['category/product', 'id' => $item['product_id']])?></span><br>
            <span class="art">Артикул <?=$item['model']?></span>
            <?if(!empty($item['option'])):?>
              <div class="options"></div>
              <?foreach ($item['option'] as  $option):?>
                <span class="art"><?=$option['option_name'] .": " . $option['option_value']?>; </span>
              <?endforeach?>
            <?endif?>
          </td>
          <td class="cart-price f12" >
            <div class="<?if(isset($promocode)) echo 'decoration'?>">
          <span class="single-price f12">
            <?= Yii::$app->formatter->asDecimal($item['price']) ?>
          </span>
              <span class="curr f12"> грн</span>
            </div>
            <? if ($promocode): ?>
              <div class="new-price f12">
                <span><?=$item['price_with_discount']?></span>
                <span class="curr f12"> грн</span>
              </div>
            <? endif ?>
          </td>
          <? if($promocode):?>
            <td class="discount">
          <span class="sum f12">
            -<?=$item['discount_sum'] * $item['quantity']; $total_cart_discount += $item['discount_sum'] * $item['quantity'];?>
          </span><span> грн</span>
            </td>
          <?endif?>
          <td class="count">
            <?=$item['quantity']?>
          </td>
          <td class="total f12">
        <span>
          <?=$item['quantity'] * (($item['discount']) ? $item['price_with_discount'] : $item['price'])?>
        </span>
            <span class="curr"> ₴</span>
          </td>
        </tr>
        <?endforeach?>
        </tbody>
      </table>
    </div>
  </div>

  <div id="total-table-wrapper">
    <div id="total-table">
      <div class="row">
        <div class="td">За все товары:</div>
        <div class="td"><span id="main_total"><?=Yii::$app->formatter->asDecimal(\Yii::$app->cart->total + $total_cart_discount)?></span><span> ₴<span></div>
      </div>
      <? if ($promocode): ?>
        <div class="row">
          <div class="td">Купон:</div>
          <div class="td"><?=$promocode->str_code?> (<?=$promocode->code_percent?>%)</div>
        </div>
        <div class="row">
          <div class="td">Скидка по купону:</div>
          <div class="td" id="discount-total">
            <span>-<?=$total_cart_discount?></span> грн
          </div>
        </div>
      <? endif ?>
      <div class="row">
        <div class="td">Доставка:</div>
        <div class="td">0 ₴</div>
      </div>
      <div class="row total-row">
        <div class="td total-title">Итого:</div>
        <div class="td total-sum"><span><?=\Yii::$app->cart->total?></span> ₴</div>
      </div>
    </div>
  </div>
  <div class="clear"></div>

  <div class="social-block">
    <p>Присоеденяйтесь к нам в социальных сетях. Свежие новости, розыгрыши и много всего интересного!</p>
    <a href="" title="Вконтакте" class="vk social-icon"></a>
    <a href="" title="Facebook" class="fb social-icon"></a>
    <a href="" title="Twitter" class="tw social-icon"></a>
    <a href="" title="Google+" class="go social-icon"></a>
  </div>
  <div class="pluso" data-background="transparent" data-options="big,round,line,horizontal,nocounter,theme=08" data-services="vkontakte,facebook,twitter,google"></div>
  <?=Html::a('Вернуться в каталог', ['category/category'], ['id' => 'confirm-btn'])?>
</div>

