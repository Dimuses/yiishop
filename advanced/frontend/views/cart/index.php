<?php
/* @var $items array элементы в корзине */
/* @var $promocode \frontend\models\ActiveRecord\DiscountCode*/

use frontend\assets\FormstoneAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\widgets\Alert;

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = $this->title;
$total_cart_discount = 0;

FormstoneAsset::register($this)?>

<div id="main-cart">
  <h1>Корзина</h1>
<?=Alert::widget();?>
  <?if(!empty($items)):?>
  <table class="cart" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
      <td></td>
      <td></td>
      <td>Цена</td>
      <? if($promocode):?><td class="discount">Скидка(<?=$promocode->code_percent?>%)</td><?endif?>
      <td>Количество</td>
      <td>Итого</td>
      <td></td>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($items as $item):?>
    <tr id="<?=$item['cart_id']?>">
      <td><img width="<?=Yii::$app->params['picSize']['cart']['w']?>" src="<?= Yii::getAlias('@web') . '/img/' . $item['image']?>" alt=""></td>
      <td class="main-info">
        <span class="prd-title"><?=Html::a(Html::encode($item['name']),['category/product', 'id' => $item['product_id']])?></span><br>
        <span class="art">Артикул <?=$item['model']?></span>
        <?if(!empty($item['option'])){?>
          <div class="options"></div>
          <?foreach ($item['option'] as  $option){?>
            <span class="art"><?=$option['option_name'] .": " . $option['option_value']?>; </span>
          <?}
        }
        ?>
      </td>
      <td class="cart-price " >
        <div class="<?if(isset($promocode)) echo 'decoration'?>">
          <span class="single-price">
            <?= Yii::$app->formatter->asDecimal($item['price']) ?>
          </span>
          <span class="curr"> грн</span>
        </div>

        <? if ($promocode): ?>
          <div class="new-price">
            <span><?=$item['price_with_discount']?></span>
            <span class="curr"> грн</span>
          </div>
        <? endif ?>
      </td>
      <? if($promocode):?>
        <td class="discount">
          <span class="sum">
            -<?=$item['discount_sum'] * $item['quantity']; $total_cart_discount += $item['discount_sum'] * $item['quantity'];?>
          </span>
          <span> грн</span>
        </td>
      <?endif?>
      <td class="count">
        <div>
          <?=Html::input('number','count', $item['quantity'], ['min'=> 1,'max'=>1000])?>
        </div>
        <div id="refresh" onclick="updateCart()">
          <i class="fa fa-refresh"></i>
        </div>
      </td>
      <td class="total">
        <span>
          <?=$item['quantity'] * (($item['discount']) ? $item['price_with_discount'] : $item['price'])?>
        </span>
        <span class="curr"> ₴</span>
      </td>
      <td class="del">
        <?=Html::a('&times;','', ['onclick' =>'deleteFromCart('. $item['cart_id'].'); return false;'])?>
      </td>
    </tr>
    <?endforeach?>
    </tbody>
  </table>

  <div class="clear"></div>
  <div id="cart-form">
    <?php
    $form = ActiveForm::begin([
      'id'=> 'order-form',
      'action' => 'checkout'
    ]);
    ?>

      <div class="first">
        <?=$form->field($model, 'name', ['template'=>'{input}{error}', 'options' =>['tag' => 'span']])
          ->textInput(['placeholder' => $model->getAttributeLabel('name'), 'class' => ''])
          ->label(false)?>

        <?=$form->field($model, 'phone', ['template'=>'{input}{error}', 'options' =>['tag' => 'span']])
          ->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '(999)-999-99-99',
            'options'=>[
              'placeholder' => $model->getAttributeLabel('phone'),
            ]
          ])
          //->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'class' => ''])
          ->label(false)?>

        <?=$form->field($model, 'email', ['template'=>'{input}{error}', 'options' =>['tag' => 'span']])
          ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => ''])
          ->label(false)?>

        <?=$form->field($model, 'orderComment', ['template'=>'{input}{error}', 'options' =>['tag' => 'span']])
          ->textarea(['placeholder' => $model->getAttributeLabel('orderComment'), 'class' => '', 'cols' => 30, 'rows' => 10])
          ->label(false)?>
      </div>
      <div class="second">

        <?=$form->field($model, 'city', ['template'=>'{input}{error}', 'options' =>['tag' => 'span']])
          ->textInput(['placeholder' => $model->getAttributeLabel('city'), 'class' => ''])
          ->label(false)?>

        <?=$form->field($model, 'street', ['template'=>'{input}{error}', 'options' =>['tag' => 'span']])
          ->textInput(['placeholder' => $model->getAttributeLabel('street'), 'class' => ''])
          ->label(false)?>

        <div class="building">

          <?=$form->field($model, 'house', ['template'=>'{input}', 'options' =>['tag' => 'span']])
            ->textInput(['placeholder' => $model->getAttributeLabel('house'), 'class' => ''])
            ->label(false)?>

          <?=$form->field($model, 'building', ['template'=>'{input}', 'options' =>['tag' => 'span']])
            ->textInput(['placeholder' => $model->getAttributeLabel('building'), 'class' => ''])
            ->label(false)?>

          <?=$form->field($model, 'apartment', ['template'=>'{input}', 'options' =>['tag' => 'span']])
            ->textInput(['placeholder' => $model->getAttributeLabel('apartment'), 'class' => ''])
            ->label(false)?>
        </div>

        <div id="dev-method">
          <?
            $delivery = [
              ['id' => 1, 'name' => 'Доставка Курьером', 'info' => 'Срок доставки от 1 до 2 дней. Бесплатная доставка по Киеву'],
              ['id' => 2, 'name' => 'Доставка УкрПочтой', 'info' =>''],
              ['id' => 3, 'name' => 'Доставка Новой Почтой', 'info' =>'Цену можете узнать на сайте НП'],
              ['id' => 4, 'name' => 'Самовывоз', 'info' => 'Возможен из нашего офису по адресу Генерала Петрова, с 8:00 - 21:00'],
            ];

          $this->params['delivery'] = $delivery;
          ?>
          <?=$form->field($model, 'delivery_id')
            ->radioList(ArrayHelper::map($delivery, 'id', 'name'),
            [
              'item' => function($index, $label, $name, $checked, $value) {
              $return = "<label>";
              $return .= "<input type='radio' name='$name' value='$value'>";
              $return .=  ucwords($label);
              $return .= "</label>";
              $return .= (!empty($this->params['delivery'][$index]['info'])) ? "<span class='info'>{$this->params['delivery'][$index]['info']}</span>" : '';
              return $return;
             }
          ])->label('Способ доставки:')?>
        </div>
      </div>

      <div class="third">
        <p class="error"><?=Yii::$app->session->getFlash('error');?></p>
        <input id="promo-code" name="CartForm[coupon_code]" type="text" placeholder="Промокупон или купон" value="<?=$promocode->str_code?>"><span id="ok-btn" onclick="usePromoCode()">Ок</span>
        <input type="text" placeholder="Дисконтная карта">
        <span class="info">С вами свяжется менеджер и вы получите дополнительную скидку</span>

        <div class="bg-dashed">
          <?
          $payment_imgs = [
             'uah.png', 'visa.jpg'
          ];
          $this->params['payment_img'] = $payment_imgs;
          ?>
          <?=$form->field($model,'payment_id')
            ->radioList(['1' =>' Наличными', '2' => ' Банковской картой'],
              [
                'item' => function($index, $label, $name, $checked, $value) {
                  $return = "<label>";
                  $return .= "<input type='radio' name='$name' value='$value'>";
                  $return .=  ucwords($label);
                  $return .=  (!empty($this->params['payment_img'][$index])) ? " <img class='img-payment' height='15' src='". Yii::getAlias('@web') ."/img/{$this->params['payment_img'][$index]}'>" : '';
                  $return .= "</label>";
                  return $return;
                }
              ])->label('Способ оплаты:')?>
        </div>
        <div id="total-table">
          <div class="row">
            <div class="td">За все товары:</div>
            <div class="td"><span id="main_total"><?=Yii::$app->formatter->asDecimal(\Yii::$app->cart->total + $total_cart_discount)?></span><span> ₴<span></div>
          </div>

          <? if ($promocode): ?>
            <div class="row">
              <div class="td">Купон:</div>
              <div class="td"><?=$promocode->str_code?> (<?=$promocode->code_percent?>%)</div>
            </div>
            <div class="row">
              <div class="td">Скидка по купону:</div>
              <div class="td" id="discount-total">
                <span>-<?=$total_cart_discount?></span> грн
              </div>
            </div>
          <? endif ?>

          <div class="row">
            <div class="td">Доставка:</div>
            <div class="td">0 ₴</div>
          </div>
          <div class="row total-row">
            <div class="td total-title">Итого:</div>
            <div class="td total-sum"><span><?=\Yii::$app->cart->total?></span> ₴</div>
          </div>

        </div>
        <div class="td">
          <?=Html::submitButton('Отправить заказ', ['id' => 'confirm-btn'])?>
          <!--<div id="confirm-btn">Оформить заказ</div>-->
        </div>
      </div>
      <div class="clear"></div>

    <?ActiveForm::end()?>
    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
<?endif?>

<?
$delFunc = <<<JS
function deleteFromCart(id){
    var total_qty = 0;
    var total_sum = 0;
    var total_discount = 0;

    $('#' + id).fadeOut(500, function(){
        $(this).detach();
    });
    $.post('delete?id=' + id, function(data){
      console.log(data);
      $.each(data, function(i, e){
        var tr =  $('#' + e.cart_id);
        var qty = tr.find('.fs-number-element').val();
        var price = (e.discount) ? e.price_with_discount  : e.price;
        total_qty += Number(qty);
        total_sum += Number(qty * price) ;

        if(e.discount){
          //Формируем общую сэкономленную сумму;
          total_discount += Number(e.discount_sum * qty);
        }
      });

      $('#cnt-total .qty').fadeOut(100, function(){
        $(this).html(total_qty).fadeIn();
      });
      $('#cnt-total .total').fadeOut(100, function(){
        $(this).html(total_sum.toFixed(2)).fadeIn();
      });
      $('.td.total-sum span').fadeOut(100, function(){
        $(this).html(total_sum.toFixed(2)).fadeIn();
      });
      //Сумма без скидок, получаем сложением скидки и обшей суммы
      $('#main_total').fadeOut(100, function(){
        $(this).html((total_sum + total_discount).toFixed(2)).fadeIn();
      });
      $('#discount-total span').fadeOut(100, function(){
        $(this).html(-total_discount.toFixed(2)).fadeIn();
      });
    },'json');
  }
JS;

$updateFunc = <<<JS
function updateCart(){

  var cart = [];
  //Перебираем все элементы в корзине и формируем массив для запроса
  $('.cart tr[id]').each(function(i, e){
    var item = {};
    item.id = $(e).attr('id');
    item.qty = $(e).find('.count input').val();

    cart.push(item);
  });

  $.post('update', {cart: cart}, function(data){
    console.log(data);
    var total_discount = 0;
    var total_sum = 0;
    var total_count =0;

    $.each(data, function(i, e){

      //Итого по элементу в корзине
      var tr =  $('#' + e.cart_id);
      var total_field = tr.find('.total span:nth-child(1)');
      var product_price = (e.discount) ? e.price_with_discount  : e.price;

      //Вывод общей суммы по товару
      var count_of_product = tr.find('.fs-number-element').val();
      var total_of_product = count_of_product * product_price;
      total_count += Number(count_of_product);

      if(Number(total_field.html()) != (total_of_product).toFixed(2)){
        total_field.fadeOut(100, function(){
            $(this).html((total_of_product).toFixed(2)).fadeIn();
        });
      }

      //Формируем общую сумму в корзине
      total_sum += Number((total_of_product));

      //Если есть скидка, выводим сэкономленную сумму по товару
      if(e.discount){
        var total_discount_product = -(e.discount_sum * count_of_product);

        var elDiscount = tr.find('.sum');
        if(Number(elDiscount.html()) != total_discount_product.toFixed(2)){
          elDiscount.fadeOut(100, function(){
            $(this).html(total_discount_product.toFixed(2)).fadeIn();
          });
        }
        //Формируем общую сэкономленную сумму;
        total_discount += total_discount_product;
      }
    });

    //Обновляем в корзине в шапке
    var header_cart_qty = $('#cnt-total .qty');

    if(header_cart_qty.html() != total_count){
      header_cart_qty.fadeOut(100, function(){
        $(this).html(total_count).fadeIn();
      });
    }
    var header_cart_total = $('#cnt-total .total');

    if(header_cart_total.html() != total_sum.toFixed(2)){
      header_cart_total.fadeOut(100, function(){
        $(this).html(total_sum.toFixed(2)).fadeIn();

      });
    }

    //Общая сумма с учетом всех скидок
    var cart_total_discount =  $('.td.total-sum span');

    if(Number(cart_total_discount.html()) != total_sum.toFixed(2)){
      cart_total_discount.fadeOut(100, function(){
        $(this).html(total_sum.toFixed(2)).fadeIn();
      });
    }

    //Общая сэкономленная сумма по всем товарам
    var total_discount_span = $('#discount-total span');
    if(total_discount_span.html() != total_discount.toFixed(2)){
      total_discount_span.fadeOut(100, function(){
        $(this).html(total_discount.toFixed(2)).fadeIn();
      });
    }

    //Сумма без скидок, получаем сложением скидки и обшей суммы
    var tot_without_disc = total_sum + -(total_discount.toFixed(2));

    if($('#main_total').html() != tot_without_disc.toFixed(2)){
      $('#main_total').fadeOut(100, function(){
        $(this).html(tot_without_disc.toFixed(2)).fadeIn();
      });
    }
  }, 'json');
}
JS;

$codeFunc = <<<JS
function usePromoCode(){
  var code = $('#promo-code').val();
  $.post('discount', {code: code}, function(data){
    console.log(data);
  });
}
JS;

$formStone = <<<JS
$("input[type=checkbox], input[type=radio]").checkbox({customClass: "cust-check"});
$("input[type=number]").number({theme: "fs-light"});
JS;
?>

<?php $this->registerJs($delFunc, View::POS_END)?>
<?php $this->registerJs($updateFunc, View::POS_END)?>
<?php $this->registerJs($formStone, View::POS_END)?>
<?php $this->registerJs($codeFunc, View::POS_END)?>