<?php

namespace frontend\models;

use yii\base\Model;


class CategoryForm extends Model{

  public $perPage;
  public $sortBy;
  public $viewPage = '_categoryGrid';

  public function rules()
  {
    return [
      [['perPage', 'sortBy', 'viewPage'], 'required']
    ];
  }

  public function attributeLabels()
  {
    return [
      'perPage' => 'Отображать по:',
      'sortBy' => 'Сортировка по',
      'viewPage' => 'Вид',

    ];
  }

}