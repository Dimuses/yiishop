<?php
namespace frontend\models\query;
use yii\db\ActiveQuery;

class MenuQuery extends ActiveQuery
{
  public function top($state = true)
  {
    return $this->select(['name','description','view'])
      ->leftJoin('category_description', '`category`.`category_id` = `category_description`.`category_id`')
      ->andWhere(['top' => $state])
      ->asArray()
      ->all();
  }
}