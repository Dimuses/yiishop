<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 06.04.2016
 * Time: 17:07
 */

namespace frontend\models;


use yii\base\Model;

class CartForm extends Model
{
  public $name;
  public $phone;
  public $email;
  public $orderComment;
  public $city;
  public $street;
  public $house;
  public $building;
  public $apartment;
  public $delivery_id;
  public $payment_id;
  public $coupon_code;

  public function rules()
  {
    return [
      [['name', 'phone', 'email', 'city', 'street', 'house' , 'building', 'apartment', 'delivery_id', 'payment_id'], 'required', 'message' => 'Поле "{attribute}" не может быть пустым'],
      ['email', 'email', 'message' => 'Введите корректный email адресс'],
    ];
  }

  public function attributeLabels()
  {
    return [
      'name' => 'Имя',
      'phone' => 'Телефон',
      'email' => 'E-mail',
      'orderComment' => 'Комментарий к заказу',
      'city' => 'Город',
      'street' => 'Улица',
      'house' => 'Дом',
      'building' => 'Стр.',
      'apartment' => 'Кв.',
      'delivery_id' => 'Доставка',
      'payment_id' => 'Оплата',
    ];
  }


}