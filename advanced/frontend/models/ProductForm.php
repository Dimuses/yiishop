<?php

namespace frontend\models;

use frontend\models\ActiveRecord\Product;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;


class ProductForm extends Model
{
  public $product_id;
  public $dynamicAttributes = [];


  public function __get($name)
  {
    if(isset($this->dynamicAttributes[$name])) {
      return $this->dynamicAttributes[$name];
    }
    return null;
  }

  public function __set($name,$value)
  {
    if(!isset($this->dynamicAttributes[$name])) {
      $this->dynamicAttributes[$name] = $value;
    }
  }

  public function attachOptionsRules($options)
  {
    $validators = $this->getValidators();
    foreach ($options as  $option) {
      if($option['required'])
        $validators[] = \yii\validators\Validator::createValidator('required', $this, 'option_'. $option['product_option_id'], ['message' => 'Выберите "' . $option['name'] . '"']);
    }
  }
}