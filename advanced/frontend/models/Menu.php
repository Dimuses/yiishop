<?php
namespace frontend\models;

use frontend\models\query\MenuQuery;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class Menu extends ActiveRecord{
  public static function tableName()
  {
    return 'category';
  }

  public static function find(){
    return new MenuQuery(get_called_class());
  }

  public function top(){
    return $this->prepare(static::find()->top());
  }

  public function prepare($arr){
    $result = [];
    foreach ($arr as $item) {
      $result[] = [
        'label' => $item['name'],
        'url' => ['site/page', 'view' => $item['view']],
      ];
    }

    return $result;
  }
}

