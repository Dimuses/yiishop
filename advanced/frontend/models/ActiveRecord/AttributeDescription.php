<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "attribute_description".
 *
 * @property integer $attribute_id
 * @property integer $language_id
 * @property string $name
 */
class AttributeDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribute_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'language_id', 'name'], 'required'],
            [['attribute_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => 'Attribute ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
        ];
    }
}
