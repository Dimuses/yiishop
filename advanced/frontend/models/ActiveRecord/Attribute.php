<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "attribute".
 *
 * @property integer $attribute_id
 * @property integer $attribute_group_id
 * @property integer $sort_order
 */
class Attribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_group_id', 'sort_order'], 'required'],
            [['attribute_group_id', 'sort_order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => 'Attribute ID',
            'attribute_group_id' => 'Attribute Group ID',
            'sort_order' => 'Sort Order',
        ];
    }
    public function getAttributeDescription(){
        return $this->hasmany(AttributeDescription::className(),['attribute_id' => 'attribute_id']);
    }
}
