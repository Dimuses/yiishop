<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "category_description".
 *
 * @property integer $category_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $view
 */
class CategoryDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'language_id', 'name', 'description', 'meta_description', 'meta_keyword', 'seo_title', 'seo_h1', 'view'], 'required'],
            [['category_id', 'language_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'meta_description', 'meta_keyword', 'seo_title', 'seo_h1', 'view'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
            'description' => 'Description',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'view' => 'View',
        ];
    }
    public function getCategory(){
      return $this->hasOne(Category::className(),['category_id' => 'category_id']);
    }
}
