<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "sticker".
 *
 * @property integer $sticker_id
 * @property integer $language_id
 * @property string $name
 * @property string $image
 * @property string $color
 */
class Sticker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sticker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id'], 'integer'],
            [['name', 'image', 'color'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sticker_id' => 'Sticker ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
            'image' => 'Image',
            'color' => 'Color',
        ];
    }
}
