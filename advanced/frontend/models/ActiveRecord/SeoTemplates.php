<?php

namespace frontend\models\ActiveRecord;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "seo_templates".
 *
 * @property integer $seo_template_id
 * @property string $seo_template
 * @property string $entity
 * @property integer $entity_id
 */
class SeoTemplates extends \yii\db\ActiveRecord
{

  /**
   * @var array Сущности которые являются элементами
   */
    protected $_entity_elements = ['product'];

  /**
   * @var array Сущности которые являются разделами
   */
    protected $_entity_sections = ['category'];

  /**
   * @var string Имя элемента для подстановки
   */
    protected $_element_name;
  /**
   * @var string Цена элемента для подстановки
   */
    protected $_element_price;
  /**
   * @var array Все доступные атрибуты элемента
   */
    protected $_element_attributes;
  /**
   * @var string Имя раздела
   */
    protected $_section_name;
  /**
   * @var string Имя Родительского раздела
   */
    protected $_parent_section_name;


  /**
   * Числовой идентификатор для элементов в БД
   */
    const SEO_ELEMENT = '0';
  /**
   * Числовой идентификатор для разделов в БД
   */
    const SEO_SECTION = '1';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['seo_template', 'entity'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'seo_template_id' => 'Seo Template ID',
            'seo_template' => 'Seo Template',
            'entity' => 'Entity',
            'entity_id' => 'Entity ID',
        ];
    }

  /**
   *
   */
    public function create()
    {
        $this->entity = hash('crc32', get_class($this->model));
        $this->entityId = $this->model->{$this->entityIdAttribute};
    }

  /**
   * @param $type string Тип сущности
   * @param $id int Id сущности
   * @return array Возвращает готовые шаблоны в контроллер
   */
    public function getReadyTemplates($type, $id)
    {
        $template = $this->getTemplate($type, $id);

        if(in_array($type, $this->_entity_elements))
            $this->getItemAttributes($id);

        $readyTitle = $this->applyReplace($template->title_template);
        $readyDescription = $this->applyReplace($template->description_template);
        $readyKeywords = $this->applyReplace($template->keywords_template);

        $metaTags = [];

        //Чистим от пробелов
        $readyTitle = preg_replace('#\s{2,}#', ' ', $readyTitle);
        $readyDescription = preg_replace('#\s{2,}#', ' ', $readyDescription);
        $readyKeywords = preg_replace('#\s{2,}#', ' ', $readyKeywords);

        // Чистим от возможных неиспользованных подстановок
        $metaTags['title'] = preg_replace('#\[\[\w+\]\]#', '', $readyTitle);
        $metaTags['meta']['description'] = preg_replace('#\[\[\w+\]\]#', '', $readyDescription);
        $metaTags['meta']['keywords'] = preg_replace('#\[\[\w+\]\]#', '', $readyKeywords);

        return $metaTags;
    }

  /**
   * @param $string string Сырая строка с тегами подстановки
   * @return mixed Обработанная строка
   */
    public function applyReplace(&$string)
    {
        preg_match_all('#\[\[(\w+)\]\]#', $string, $matches);

        foreach ($matches[1] as $match) {
            switch($match){
                case 'NAME' :
                    $string = str_replace('[['. $match . ']]' , $this->_element_name , $string); break;
                case 'PARENT_SECTION_NAME' :
                    $string = str_replace('[['. $match . ']]' , $this->_parent_section_name , $string); break;
                case 'SECTION_NAME' :
                    $string = str_replace('[['. $match . ']]' , $this->_section_name , $string); break;
                case 'PRICE' :
                    $string = str_replace('[['. $match . ']]' , $this->_element_price , $string); break;
                default:
                    if(array_key_exists($match, $this->_element_attributes))
                        $string = str_replace('[['. $match . ']]' , $this->_element_attributes[$match] , $string);
                    break;
            }
        }
        return $string;
    }

  /**
   * @param $type string Тип сущности
   * @param $id int идентификатор сущности
   * @return array|null|\yii\db\ActiveRecord
   */
    public function getTemplate($type, $id){

        if(in_array($type, $this->_entity_elements))
            $template_type_id = self::SEO_ELEMENT;
        else
            $template_type_id = self::SEO_SECTION;

        $result = self::find()
          ->where([
            'entity' => $type,
            'entity_id' => $id,
            'template_type_id' => $template_type_id
          ])
          ->one();

        switch($type){
            case 'product':
                $element = Product::find()->where(['product_id' => $id])->one();

                $this->_element_name = preg_replace('#\s{2,}#', ' ', $element->productDescription->name);
                $this->_element_price = Yii::$app->formatter->asDecimal($element->price);

                $maxCategory = ProductToCategory::find()->where(['product_id' => $id])->max('category_id');
                $category = Category::find()->where(['category_id' => $maxCategory])->one();
                $this->_section_name = $category->categoryDescription->name;

                if(empty($result))
                    $result = $this->getTemplateRecursion('category', $maxCategory, $template_type_id);
                break;
            case 'category':
                //TODO
                break;
        }
        return $result;
    }

  /**
   * @param $type string Тип сущности
   * @param $category_id int Идентификатор категории
   * @param $template_type_id int Идентификатор типа шаблона(Элемент, раздел)
   * @return array|null|\yii\db\ActiveRecord
   *
   * Получаем рекурсивно шаблоны заданные у родителей. Ищется первый найденный шаблон
   */
    public function getTemplateRecursion($type, $category_id, $template_type_id )
    {
        static $i = 0;

        $result = self::find()
          ->where([
            'entity' => $type,
            'entity_id' => $category_id,
            'template_type_id' => $template_type_id
          ])
          ->one();

        if(empty($result)){
            $category = Category::find()->where(['category_id' => $category_id])->one();

            if(!empty($category->parent_id) && $category->parent_id !== 0){
                $result = $this->getTemplateRecursion($type, $category->parent_id, $template_type_id);
            }
        }else{
            if($i == 0){
                $category = Category::find()->where(['category_id' => $category_id])->one();
                $this->_parent_section_name = $category->categoryDescription->name;
            }
        }
        return $result;
    }

  /**
   * @param $item_id int Идентификатор элемента
   * @return array Формат A_NAME => VALUE
   *
   * Возвращает все атрибуты элемента, заданные в таблице Product_attributes
   */
    public function getItemAttributes($item_id)
    {
        $attributes = ProductAttribute::find()
          ->joinWith('attributeInfo')
          ->where(['product_id' => $item_id])
          ->asArray()
          ->all();

        $preparedArray = [];

        foreach ($attributes as  $i) {
            $preparedArray[$i['attributeInfo'][0]['attribute_code']] = $i['text'];
        }

        $this->_element_attributes = $preparedArray;
        return $preparedArray;
    }
}
