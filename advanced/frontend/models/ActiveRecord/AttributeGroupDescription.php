<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "attribute_group_description".
 *
 * @property integer $attribute_group_id
 * @property integer $language_id
 * @property string $name
 */
class AttributeGroupDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribute_group_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_group_id', 'language_id', 'name'], 'required'],
            [['attribute_group_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_group_id' => 'Attribute Group ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
        ];
    }
}
