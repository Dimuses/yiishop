<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "layout".
 *
 * @property integer $layout_id
 * @property string $name
 */
class LayoutDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'layout_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'layout_id' => 'Layout ID',
            'name' => 'Name',
        ];
    }

    public function getLayoutRoute()
    {
        return $this->hasOne(LayoutRoute::className(),['layout_id' => 'layout_id']);
    }
}
