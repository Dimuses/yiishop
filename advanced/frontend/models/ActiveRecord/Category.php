<?php

namespace frontend\models\ActiveRecord;

use frontend\models\ActiveRecord\CategoryDescription;
use Yii;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category".
 *
 * @property integer $category_id
 * @property string $image
 * @property integer $parent_id
 * @property integer $top
 * @property integer $column
 * @property integer $sort_order
 * @property integer $status
 * @property string $date_added
 * @property string $date_modified
 */
class Category extends ActiveRecord
{
    /**
     * @var null|array|ActiveRecord[] Category children
     */
    protected $_children;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'top', 'column', 'sort_order', 'status'], 'integer'],
            [['top', 'column', 'status'], 'required'],
            [['date_added', 'date_modified'], 'safe'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'image' => 'Image',
            'parent_id' => 'Parent ID',
            'top' => 'Top',
            'column' => 'Column',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'date_added' => 'Date Added',
            'date_modified' => 'Date Modified',
        ];
    }
    public function getCategoryDescription(){
      return $this->hasOne(CategoryDescription::className(),['category_id' => 'category_id']);
    }

    public function getTree()
    {
        return $this->buildTree(self::find()
          ->with('categoryDescription')
          ->where(['top' => '0'])
          ->orderBy(['parent_id' => SORT_ASC, 'sort_order' => SORT_ASC])
          ->all());
    }

    protected static function buildTree(&$data, $rootID = 0)
    {
        $tree = [];
        foreach ($data as $id => $node) {
            if ($node->parent_id == $rootID) {
                unset($data[$id]);
                $node->children = self::buildTree($data, $node->category_id);
                $tree[] = $node;
            }
        }
        return $tree;
    }

    /**
     * $_children getter.
     *
     * @return null|array|ActiveRecord[] Category children
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * $_children setter.
     *
     * @param array|ActiveRecord[] $value Category children
     */
    public function setChildren($value)
    {
        $this->_children = $value;
    }

    public function prepareMenuItems()
    {
        $tree = $this->getTree();
        return $menuItems = $this->buildMenuItems($tree);
    }

    public function buildMenuItems($tree)
    {
        $menuItems = [];
        $menuItem = [];
        foreach ($tree as  $item) {
            $menuItem['label'] = $item->categoryDescription->name;
            $menuItem['url'] = ['category/category', 'id' => $item->category_id];
            if(!empty($item->children)){
                $menuItem['items'] = $this->buildMenuItems($item->children);
            }
            $menuItems[] = $menuItem;
            unset($menuItem);
        }
        return $menuItems;
    }

    public function prepareBreadcrumbs($category_id)
    {
        $breadcrumbs = $this->getParentCategories($category_id);
        $last = array_pop($breadcrumbs);
        unset($last['url']);

        $breadcrumbs[]= $last;
        return $breadcrumbs;
    }

    public function getParentCategories($category_id)
    {
        $breadcrumbs = [];
        $breadcrumbItem = [];

        $item = (new Query())
          ->select(['c.category_id', 'name', 'parent_id'])
          ->from('category c')
          ->innerJoin('category_description cd', 'cd.category_id = c.category_id')
          ->where(['c.category_id' => $category_id])
          ->one();

        if(!empty($item['parent_id'])){
            $breadcrumbs = $this->getParentCategories($item['parent_id']);
        }

        $breadcrumbItem['url'] = ['category/category', 'id' => $item['category_id']];
        $breadcrumbItem['label'] = $item['name'];
        $breadcrumbs[] = $breadcrumbItem;

        return $breadcrumbs;
    }

    public function getSearchedIds($results){
        $productIds = [];
        foreach ($results as $result) {
            $productIds[] = $result->product_id;
        }
        return $productIds;
    }
}
