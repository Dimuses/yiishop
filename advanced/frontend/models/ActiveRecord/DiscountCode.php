<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "discount_code".
 *
 * @property integer $id_code
 * @property string $str_code
 * @property string $start_date
 * @property string $exp_date
 * @property integer $max_using
 * @property integer $used
 * @property integer $code_percent
 */
class DiscountCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'exp_date'], 'safe'],
            [['exp_date', 'max_using', 'used', 'code_percent'], 'required'],
            [['max_using', 'used', 'code_percent'], 'integer'],
            [['str_code'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_code' => 'Id Code',
            'str_code' => 'Str Code',
            'start_date' => 'Start Date',
            'exp_date' => 'Exp Date',
            'max_using' => 'Max Using',
            'used' => 'Used',
            'code_percent' => 'Code Percent',
        ];
    }

    public function isActive()
    {
        return $this->start_date < date('Y-m-d') && date('Y-m-d') < $this->exp_date;
    }
}
