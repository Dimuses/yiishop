<?php

namespace frontend\models\ActiveRecord;

use Yii;
use \yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "product".
 *
 * @property integer $product_id
 * @property string $model
 * @property string $sku
 * @property string $upc
 * @property string $ean
 * @property string $jan
 * @property string $isbn
 * @property string $mpn
 * @property string $location
 * @property integer $quantity
 * @property integer $stock_status_id
 * @property string $image
 * @property integer $manufacturer_id
 * @property integer $shipping
 * @property string $price
 * @property integer $points
 * @property integer $tax_class_id
 * @property string $date_available
 * @property string $weight
 * @property integer $weight_class_id
 * @property string $length
 * @property string $width
 * @property string $height
 * @property integer $length_class_id
 * @property integer $subtract
 * @property integer $minimum
 * @property integer $sort_order
 * @property integer $status
 * @property string $date_added
 * @property string $date_modified
 * @property integer $viewed
 */
class Product extends \yii\db\ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'product';
  }

  public function scenarios()
  {
    $scenarios = parent::scenarios();
    $scenarios['addToCart'] = ['product_id'];
    return $scenarios;
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['model', 'sku', 'upc', 'ean', 'jan', 'isbn', 'mpn', 'location', 'stock_status_id', 'manufacturer_id', 'tax_class_id', 'date_available'], 'required'],
      [['quantity', 'stock_status_id', 'manufacturer_id', 'shipping', 'points', 'tax_class_id', 'weight_class_id', 'length_class_id', 'subtract', 'minimum', 'sort_order', 'status', 'viewed'], 'integer'],
      [['price', 'weight', 'length', 'width', 'height'], 'number'],
      [['date_available', 'date_added', 'date_modified'], 'safe'],
      [['model', 'sku', 'mpn'], 'string', 'max' => 64],
      [['upc'], 'string', 'max' => 12],
      [['ean'], 'string', 'max' => 14],
      [['jan', 'isbn'], 'string', 'max' => 13],
      [['location'], 'string', 'max' => 128],
      [['image'], 'string', 'max' => 255],
      ['product_id', 'exist', 'on' => 'addToCart'],
      ['image', 'default', 'value' => function ($model, $attribute) {
        return $model->image . 'test';
      }],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'product_id' => 'Product ID',
      'model' => 'Model',
      'sku' => 'Sku',
      'upc' => 'Upc',
      'ean' => 'Ean',
      'jan' => 'Jan',
      'isbn' => 'Isbn',
      'mpn' => 'Mpn',
      'location' => 'Location',
      'quantity' => 'Quantity',
      'stock_status_id' => 'Stock Status ID',
      'image' => 'Image',
      'manufacturer_id' => 'Manufacturer ID',
      'shipping' => 'Shipping',
      'price' => 'Price',
      'points' => 'Points',
      'tax_class_id' => 'Tax Class ID',
      'date_available' => 'Date Available',
      'weight' => 'Weight',
      'weight_class_id' => 'Weight Class ID',
      'length' => 'Length',
      'width' => 'Width',
      'height' => 'Height',
      'length_class_id' => 'Length Class ID',
      'subtract' => 'Subtract',
      'minimum' => 'Minimum',
      'sort_order' => 'Sort Order',
      'status' => 'Status',
      'date_added' => 'Date Added',
      'date_modified' => 'Date Modified',
      'viewed' => 'Viewed',
    ];
  }

  public function getProductDescription()
  {
    return $this->hasOne(ProductDescription::className(), ['product_id' => 'product_id']);
  }

  public function getProductCategory()
  {
    return $this->hasmany(ProductToCategory::className(), ['product_id' => 'product_id']);
  }

  public function getProductAttributes()
  {
    return $this->hasmany(ProductAttribute::className(), ['product_id' => 'product_id']);
  }

  public function getProductStickers()
  {
    return $this->hasmany(StickerToProduct::className(), ['product_id' => 'product_id']);
  }

  public function getProductComments()
  {
    return $this->hasmany(Comment::className(), ['entityId' => 'product_id']);
  }

  public function getProductCategories()
  {
    return $this->hasmany(ProductToCategory::className(), ['product_id' => 'product_id']);
  }

  public function getProductName()
  {
    return $this->productDescription->name;
  }


  public function getItemAttributes($id)
  {
    return $attributes = (new Query())
      ->select(['agd.name a_group', 'ad.name a_name', 'pa.text val'])
      ->from(['product_attribute pa'])
      ->leftJoin('attribute a', 'a.attribute_id = pa.attribute_id')
      ->leftJoin('attribute_description ad', 'ad.attribute_id = a.attribute_id AND ad.language_id = pa.language_id')
      ->leftJoin('attribute_group ag', 'ag.attribute_group_id = a.attribute_group_id')
      ->leftJoin('attribute_group_description agd', 'agd.attribute_group_id = ag.attribute_group_id AND agd.language_id = pa.language_id')
      ->leftJoin('product p', 'pa.product_id = p.product_id')
      ->where(['p.product_id' => $id, 'pa.language_id' => 1,])
      ->all();
  }

  public function prepareAttributes($id)
  {
    $attributes = $this->getItemAttributes($id);
    $preparedAttributes = [];
    foreach ($attributes as $k => $v) {
      $preparedAttributes[$v['a_group']][$v['a_name']] = $v['val'];
    }
    return $preparedAttributes;
  }

  public function getItemOptions($id)
  {
    return $options = (new Query())
      ->select(['product_option_id', 'o.option_id', 'name', 'type', 'value', 'required'])
      ->from(['product_option po'])
      ->leftJoin('option o', 'o.option_id = po.option_id')
      ->leftJoin('option_description od', 'od.option_id = o.option_id')
      ->where(['po.product_id' => $id, 'language_id' => 1])
      ->all();
  }

  public function getOptionValues($product_id, $option_id)
  {
    return $optionValues = (new Query())
      ->select(['pov.product_option_value_id', 'pov.option_value_id', 'ovd.name', 'ov.image', 'price', 'price_prefix'])
      ->from('product_option_value pov')
      ->leftJoin('option_value_description ovd', 'ovd.option_value_id = pov.option_value_id')
      ->leftJoin('option_value ov', 'ov.option_value_id = ovd.option_value_id')
      ->where(['pov.product_id' => $product_id, 'ovd.language_id' => 1, 'pov.product_option_id' => $option_id])
      ->all();
  }

  public function prepareOptions($id)
  {
    $options = $this->getItemOptions($id);
    $preparedOptions = [];
    $preparedOption = [];

    foreach ($options as $option) {
      foreach ($option as $k => $v) {
        if (!next($option))
          $preparedOption['product_option_value'] = $this->getOptionValues($id, $option['product_option_id']);
        $preparedOption[$k] = $v;
      }
      $preparedOptions[] = $preparedOption;
    }
    return $preparedOptions;
  }

  public function getTopOrdered($category, $limit = '')
  {
    return (new Query())
    ->select(['p.product_id', 'pd.name','p.price', 'p.image','SUM(op.quantity)'])
    ->from(['order_product op'])
    ->leftJoin('product p', 'p.product_id = op.product_id')
    ->leftJoin('product_description pd', 'pd.product_id = p.product_id')
    ->leftJoin('product_to_category ptc', 'ptc.product_id = p.product_id')
    ->where(['ptc.category_id' => $category])
    ->groupBy(['op.product_id', 'pd.name'])
    ->orderBy(['SUM(op.quantity)' => SORT_DESC])
    ->limit($limit)
    ->all();
  }


}
