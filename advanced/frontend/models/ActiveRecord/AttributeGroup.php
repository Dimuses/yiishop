<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property integer $attribute_group_id
 * @property integer $sort_order
 */
class AttributeGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribute_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_order'], 'required'],
            [['sort_order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_group_id' => 'Attribute Group ID',
            'sort_order' => 'Sort Order',
        ];
    }
}
