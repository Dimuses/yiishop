<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "layout_module".
 *
 * @property integer $layout_module_id
 * @property integer $layout_id
 * @property string $code
 * @property string $position
 * @property integer $sort_order
 */
class LayoutModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'layout_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['layout_id', 'code', 'position', 'sort_order'], 'required'],
            [['layout_id', 'sort_order'], 'integer'],
            [['code'], 'string', 'max' => 64],
            [['position'], 'string', 'max' => 14]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'layout_module_id' => 'Layout Module ID',
            'layout_id' => 'Layout ID',
            'code' => 'Code',
            'position' => 'Position',
            'sort_order' => 'Sort Order',
        ];
    }

    public function getLayoutDescription()
    {
        return $this->hasOne(LayoutDescription::className(),['layout_id' => 'layout_id']);
    }


}
