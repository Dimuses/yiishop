<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entityId
 * @property string $content
 * @property integer $parentId
 * @property integer $level
 * @property integer $createdBy
 * @property integer $updatedBy
 * @property string $relatedTo
 * @property integer $status
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entityId', 'content', 'createdBy', 'updatedBy', 'relatedTo', 'createdAt', 'updatedAt'], 'required'],
            [['entityId', 'parentId', 'level', 'createdBy', 'updatedBy', 'status', 'createdAt', 'updatedAt'], 'integer'],
            [['content'], 'string'],
            [['entity'], 'string', 'max' => 10],
            [['relatedTo'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity' => 'Entity',
            'entityId' => 'Entity ID',
            'content' => 'Content',
            'parentId' => 'Parent ID',
            'level' => 'Level',
            'createdBy' => 'Created By',
            'updatedBy' => 'Updated By',
            'relatedTo' => 'Related To',
            'status' => 'Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
        ];
    }
}
