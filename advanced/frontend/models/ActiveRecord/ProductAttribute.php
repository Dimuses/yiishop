<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "product_attribute".
 *
 * @property integer $product_id
 * @property integer $attribute_id
 * @property integer $language_id
 * @property string $text
 */
class ProductAttribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute_id', 'language_id', 'text'], 'required'],
            [['product_id', 'attribute_id', 'language_id'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'attribute_id' => 'Attribute ID',
            'language_id' => 'Language ID',
            'text' => 'Text',
        ];
    }

    public function getAttributeInfo(){
        return $this->hasmany(Attribute::className(),['attribute_id' => 'attribute_id']);
    }
    public function getAttributeDescription(){
        return $this->hasmany(AttributeDescription::className(),['attribute_id' => 'attribute_id']);
    }
}
