<?php

namespace frontend\models\ActiveRecord;
use himiklab\yii2\search\behaviors\SearchBehavior;
use Yii;

/**
 * This is the model class for table "product_description".
 *
 * @property integer $product_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $tag
 */
class ProductDescription extends \yii\db\ActiveRecord
{

    function behaviors(){
        return [
          'search' => [
            'class' => SearchBehavior::className(),
            'searchScope' => function ($model) {
                /** @var \yii\db\ActiveQuery $model */
                $model->select(['name', 'description', 'product_id']);
                //$model->andWhere(['indexed' => true]);
            },
            'searchFields' => function ($model) {
                /** @var self $model */
                return [
                  ['name' => 'name', 'value' => $model->name],
                  ['name' => 'description', 'value' => strip_tags($model->description)],
                  ['name' => 'product_id', 'value' => $model->product_id]
                  // ['name' => 'model', 'value' => 'page', 'type' => SearchBehavior::FIELD_UNSTORED],
                ];
            }
          ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'language_id', 'name', 'description', 'meta_description', 'meta_keyword', 'seo_title', 'seo_h1', 'tag'], 'required'],
            [['product_id', 'language_id'], 'integer'],
            [['description', 'tag'], 'string'],
            [['name', 'meta_description', 'meta_keyword', 'seo_title', 'seo_h1'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
            'description' => 'Description',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'tag' => 'Tag',
        ];
    }
}
