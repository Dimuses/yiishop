<?php

namespace frontend\models\ActiveRecord;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "module".
 *
 * @property integer $module_id
 * @property string $name
 * @property string $code
 * @property string $setting
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'setting'], 'required'],
            [['setting'], 'string'],
            [['name'], 'string', 'max' => 64],
            [['code'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module_id' => 'Module ID',
            'name' => 'Name',
            'code' => 'Code',
            'setting' => 'Setting',
        ];
    }

    public function getLayoutModules()
    {
        return $this->hasMany(LayoutModule::className(),['module_id' => 'module_id']);
    }

    public function prepareModules($route)
    {
        $modules = self::find()
            ->joinWith('layoutModules.layoutDescription.layoutRoute')
            ->where(['route' => $route])
            ->orderBy('sort_order')
            ->all();

        $preparedModules = [];

        foreach ($modules as  $moduleItem) {
            for($i = 0; $i < count($moduleItem->layoutModules); $i++){
                //Загружаем настройки
                $settings = (array)json_decode($moduleItem->setting);
                //Контроллер модуля
                $controllerClass = '\app\modules\\'. $moduleItem->code. '\controllers\DefaultController';
                //Класс модуля
                $moduleClass = '\app\modules\\'. $moduleItem->code. '\Module';
                //Экземпляр класса модуля
                $module = new $controllerClass('default', new $moduleClass(''), $config = []);

                $preparedModules[$moduleItem->layoutModules[$i]->position][] = $module->actionIndex($settings);
            }
        }
        return $preparedModules;
    }
}
