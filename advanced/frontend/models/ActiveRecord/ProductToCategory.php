<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "product_to_category".
 *
 * @property integer $product_id
 * @property integer $category_id
 * @property integer $main_category
 */
class ProductToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id'], 'required'],
            [['product_id', 'category_id', 'main_category'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'category_id' => 'Category ID',
            'main_category' => 'Main Category',
        ];
    }
}
