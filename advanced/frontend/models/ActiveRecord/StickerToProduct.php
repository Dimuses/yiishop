<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "sticker_to_product".
 *
 * @property integer $sticker_id
 * @property integer $product_id
 */
class StickerToProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sticker_to_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sticker_id', 'product_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sticker_id' => 'Sticker ID',
            'product_id' => 'Product ID',
        ];
    }

    public function getStickerDescription(){
        return $this->hasmany(Sticker::className(),['sticker_id' => 'sticker_id']);
    }

}
