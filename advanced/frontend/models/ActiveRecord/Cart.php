<?php

namespace frontend\models\ActiveRecord;

use Yii;
use yii\base\Model;
use frontend\components\helpers\ShopHelper;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\User;

/**
 * Class Cart
 * @package frontend\models\ActiveRecord
 */
class Cart extends ActiveRecord
{
  /**
   * @return string
   */
  public static function tableName()
  {
    return 'cart';
  }

  /**
   * @return array
   */
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['date_added'],
        ],
        // если вместо метки времени UNIX используется datetime:
         'value' => new Expression('NOW()'),
      ],
    ];
  }

  /**
   * @param $session_id
   * @return array
   */
  public function getCart($session_id)
  {
    return $cart = (new Query())
      ->from('cart')
      ->where(['session_id' => $session_id])
      ->all();
  }

  /**
   * @param $options array Значения опций товара, полученных при добавлении товара в корзину. (id опции => id значения опции )
   * @return string Строка в формате Json для добавления в таблицу CART
   */
  public function prepareOptionsToCart($options)
  {
    $preparedOptions = [];
    foreach ($options as  $k => $v ) {
      $k = preg_replace('/option_/', '', $k);

      if(is_array($v)){
        $subValues = [];
        foreach ($v as $subV) {
          $subValues[] = (int)$subV;
        }
        $preparedOptions[(int)$k] = $subValues;
      }else
        $preparedOptions[(int)$k] = (int)$v;
    }
    return json_encode($preparedOptions);
  }

  /**
   * @param $ses_id string Id текущей сессии пользователя
   * @param $id int Id товара
   * @param array $options array Значения опций товара, полученных при добавлении товара в корзину. (id опции => id значения опции )
   * @param int $qty int Кол-во товара
   */
  public function addToCart($ses_id, $id, $options = [], $qty = 1)
  {

    $options = $this->prepareOptionsToCart($options);
    $product = $this::find()->where(['session_id' => $ses_id, 'product_id' => $id, 'option' => $options])->one();

    if(!empty($product)){
      $product->updateCounters(['quantity' => $qty]);
      $product->save();
    }
    else{
      $product = new Cart();
      $product->product_id = $id;
      $product->option = $options;
      $product->session_id = $ses_id;
      $product->quantity = $qty;
      $product->save();
    }
    return $item = (new Query())
      ->select(['product_id', 'name'])
      ->from('product_description')
      ->where(['product_id' => $id])
      ->one();

  }

  /**
   * @param $ses_id string Id текущей сессии пользователя
   * @param $id int Id товара
   * @throws \Exception
   */
  public function deleteFromCart($ses_id, $id)
  {
    $product = $this::findOne(['session_id' => $ses_id, 'cart_id' => $id]);
    $product->delete();
  }

  /**
   * @param $ses_id string Id текущей сессии пользователя
   * @param $items array ('cart_id' => 'qty')
   * @throws \Exception
   */
  public function updateCart($ses_id, $items)
  {

    foreach($items as $item){
      $product = $this::findOne(['session_id' => $ses_id, 'cart_id' => $item['id']]);
      $product->quantity = $item['qty'];
      $product->update();
    }
  }

  /* @var $promoCode \frontend\models\ActiveRecord\DiscountCode */
  public function prepareItems($items, $promoCode = null){
    //TODO:: Сделать чтобы в items не было лишних пар: ключ-значение
    $items = explode(',', implode(',', array_keys($items)));

    $items = (new Query())
      ->select(['p.product_id', 'price', 'image', 'pd.name', 'option','c.quantity', 'model', 'cart_id'])
      ->from('cart c')
      ->leftjoin('product p', '`p`.`product_id` = `c`.`product_id`')
      ->leftjoin('product_description pd', '`p`.`product_id` = `pd`.`product_id`')
      ->where(['c.cart_id' => $items])
      ->all();

    if(!empty($promoCode))
      $ids = ArrayHelper::getColumn($items, 'product_id');
    else
      $ids = [];

    $preparedItems = [];

    foreach($items as $item){
      if(!empty($item['option'])){
        $options = $this->getOptionValues($item['option']);
        foreach ($options as  $option) {
          $item['price'] += $option['price'];
        }
        $item['option'] = $options;
      }

      if(in_array($item['product_id'], $ids)){
        $item['discount'] = true;
        $item['price_with_discount'] = ShopHelper::sumWithDiscount($item['price'], $promoCode->code_percent);
        $item['discount_sum'] = ShopHelper::discountOfSum($item['price'], $promoCode->code_percent);
      }
      $preparedItems[] = $item;
    }
    //VarDumper::dump($preparedItems, 100, 1);
    return $preparedItems;
  }

  /**
   * @param $optionsArr
   * @return array
   */
  public function getOptionValues($optionsArr)
  {
    $optionsArr = ArrayHelper::toArray(json_decode($optionsArr));
    $options = [];

    foreach ($optionsArr as $k  => $option ) {
      if(is_array($option)){
        foreach ($option as  $subO) {
          $options[] = $subO;
        }
      }else
        $options[] = $option;
    }

    $res = (new Query())
      ->select(['pov.*', 'od.name option_name', 'ovd.name option_value','type'])
      ->from('product_option_value pov')
      ->leftJoin('option_value_description ovd', 'ovd.option_value_id = pov.option_value_id')
      ->leftJoin('option_description od', 'od.option_id = ovd.option_id AND od.language_id = ovd.language_id')
      ->leftJoin('option o', 'o.option_id = od.option_id')
      ->where(['product_option_value_id' => $options, 'ovd.language_id' => 1])
      ->all();
    return $res ;
  }

  public function saveOrder($postData, $cart)
  {
    $items = [];
    $preparedItem = [];
    $options = [];
    $preparedOption = [];
    $total = 0;

    Yii::$app->db->createCommand()
      ->insert('orders', [
        'payment_id' => $postData['payment_id'],
        'delivery_id' => $postData['delivery_id'],
        'name' => $postData['name'],
        'street' => $postData['street'],
        'apartment' => $postData['apartment'],
        'city' => $postData['city'],
        'coupon_id' => $this->getCouponId($postData['coupon_code']),
        'user_id' => Yii::$app->user ->getId(),
        'date_added' => date('Y-m-d'),
        'status_id' => 1,
      ])->execute();

    $order_id = Yii::$app->db->lastInsertID;

    foreach ($cart as  $item) {
      $preparedItem['order_id'] =  $order_id;
      $preparedItem['product_id'] =  $item['product_id'];
      $preparedItem['quantity'] =  $item['quantity'];
      $preparedItem['name'] =  $item['name'];
      $preparedItem['model'] =  $item['model'];
      $preparedItem['price'] =  $item['price'];
      $preparedItem['total'] =  $item['price'] * $item['quantity'];
      
      if(!empty($item['option'])){
        foreach ($item['option'] as $option) {
          $preparedOption['order_id'] = $order_id;
          $preparedOption['order_product_id'] = $item['product_id'];
          $preparedOption['product_option_id'] = $option['product_option_id'];
          $preparedOption['product_option_value_id'] = $option['option_value_id'];
          $preparedOption['name'] = $option['option_name'];
          $preparedOption['value'] = $option['option_value'];
          $preparedOption['type'] = $option['type'];

          $options[] = $preparedOption;
        }
      }
      $total += $preparedItem['total'];
      $items[] = $preparedItem;
    }
    //Обновляем общую сумму в заказе
    Yii::$app->db->createCommand()->update('orders',['total' => $total],['order_id' => $order_id])->execute();
    //Пакетная вставка продуктов в order_product
    Yii::$app->db->createCommand()->batchInsert('order_product',[
      'order_id',
      'product_id',
      'quantity',
      'name',
      'model',
      'price',
      'total'], $items)
      ->execute();

    //Если есть опции у тоовара, пакетная вставка опций в order_option
    if(count($options) > 0){
      Yii::$app->db->createCommand()->batchInsert('order_option',[
        'order_id',
        'order_product_id',
        'product_option_id',
        'product_option_value_id',
        'name',
        'value',
        'type'], $options)
        ->execute();
    }
    //Удаляем из корзины
    //Yii::$app->db->createCommand()->delete('cart',['session_id' => session_id()])->execute();

    $this->sendEmail();

    return $order_id;
  }

  public function getCouponId($str)
  {
    $coupon = (new Query())
      ->select(['coupon_id'])
      ->from('discount_code')
      ->where(['str_code' => $str])
      ->one();

    return ($coupon['coupon_id']) ? $coupon['coupon_id'] : '';
  }

  public function prepareOrderFields(&$fields)
  {

    $fields['delivery'] = $this->getDelivery($fields['delivery_id']);
    $fields['payment'] = $this->getPayment($fields['payment_id']);
    $fields['extra_fields'] = $this->getExtraFields();
  }

  public function getDelivery($delivery_id)
  {
     return (new Query())
      ->from('delivery')
      ->where(['delivery_id' => $delivery_id])
      ->one();
  }

  public function getPayment($payment_id)
  {
    return (new Query())
      ->from('payment')
      ->where(['payment_id' => $payment_id])
      ->one();
  }

  public function getExtraFields()
  {
    //TODO Сделать экстраполя
    $extraField = [];
    return $extraField;
  }

  public function sendEmail()
  {
    Yii::$app->mailer->compose('confirm')
      ->setFrom('from@domain.com')
      ->setTo('iron@te.net.ua')
      ->setSubject('Тема сообщения')
      /*->setTextBody('Текст сообщения')
      ->setHtmlBody('<b>текст сообщения в формате HTML</b>')*/
      ->send();

  }
}