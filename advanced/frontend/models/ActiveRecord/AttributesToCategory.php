<?php

namespace frontend\models\ActiveRecord;

use Yii;

/**
 * This is the model class for table "attributes_to_category".
 *
 * @property integer $category_id
 * @property integer $attribute_id
 */
class AttributesToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attributes_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'attribute_id', 'attribute_to_category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'attribute_id' => 'Attribute ID',
            'attribute_to_category_id' => 'Attribute to category ID',
        ];
    }

    public function getAttributesInfo()
    {
        return $this->hasOne(Attribute::className(),['attribute_id' => 'attribute_id']);
    }
}
