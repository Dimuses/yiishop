<?php


namespace frontend\assets;


use yii\web\AssetBundle;

class FormstoneAsset extends AssetBundle
{
  public $sourcePath = '@bower/formstone';
  public $css = [
    'dist/css/checkbox.css',
    'dist/css/number.css',
    'dist/css/themes/light.css',
  ];

  public $js = [
    'dist/js/core.js',
    'dist/js/checkbox.js',
    'dist/js/number.js'
  ];
  public $depends = [
    'yii\web\YiiAsset',
  ];
}